/*
Developers:
Sasa Zhang 25117151
Ruoshi Wu 27448724
Jinchen Hu 40080398

*/

CREATE DATABASE IF NOT EXISTS rzc353_2;
USE rzc353_2;

DROP TABLE IF EXISTS user;
CREATE TABLE user
(
    userId   INT         NOT NULL AUTO_INCREMENT,
    userName VARCHAR(20) NOT NULL,
    pwd      VARCHAR(20) NOT NULL,
    address  VARCHAR(40) NOT NULL,
    isAdmin  INT         NOT NULL,
    Email    VARCHAR(40) NOT NULL,
    DOB      DATE,
    isActive INT         NOT NULL,
    PRIMARY KEY (userId)
);

# 1 --> system admin
# 2 --> ass admin
# 0 --> normal user

INSERT INTO user
VALUES (100, 'admin', 'admin', '1 Av Dufort', 1, 'system@admin.com', '1989-11-11', 1),
       (101, 'Sam', 'user', '1 Av Dufort', 0, 'sam@consys.com', '1987-10-23', 1),
       (102, 'Stephane', 'user', '1 Av Dufort', 0, 'stephane@consys.com', '1990-12-21', 1),
       (103, 'Ricky', 'user', '1 Av Dufort', 0, 'ricky@consys.com', '1985-10-13', 1),
       (104, 'Thomas', 'user', '1 Av Dufort', 0, 'thomas@consys.com', '1983-12-10', 1),
       (105, 'Sammy', 'user', '1 Av Dufort', 0, 'sammy@consys.com', '1980-10-29', 1),
       (106, 'Lilian', 'user', '1 Av Dufort', 0, 'lilian@consys.com', '1991-11-23', 1),
       (107, 'Anna', 'user', '1 Av Dufort', 0, 'anna@consys.com', '1995-11-30', 1),
       (108, 'Brandon', 'user', '1 Av Dufort', 0, 'brandon@consys.com', '1988-10-22', 1),
       (109, 'Mathew', 'user', '1 Av Dufort', 0, 'mathew@consys.com', '1993-11-25', 1),
       (110, 'Nathen', 'user', '1 Av Dufort', 2, 'nathen@consys.com', '1982-12-12', 1),
       (201, 'Allison', 'user', '2 Av Linton', 0, 'allison@consys.com', '1986-12-16', 0),
       (202, 'Elsa', 'user', '2 Av Linton', 0, 'elsa@consys.com', '1981-11-21', 1),
       (203, 'Ethan', 'user', '2 Av Linton', 0, 'ethan@consys.com', '1989-10-10', 1),
       (204, 'Samantha', 'user', '2 Av Linton', 0, 'samantha@consys.com', '1990-11-12', 1),
       (205, 'Sheldon', 'user', '2 Av Linton', 0, 'sheldon@consys.com', '1989-12-22', 1),
       (206, 'Michelle', 'user', '2 Av Linton', 0, 'michelle@consys.com', '1987-10-27', 1),
       (207, 'Fred', 'user', '2 Av Linton', 0, 'fred@consys.com', '1981-12-21', 1),
       (208, 'Austin', 'user', '2 Av Linton', 0, 'austin@consys.com', '1985-12-15', 0),
       (209, 'Donald', 'user', '2 Av Linton', 0, 'donald@consys.com', '1987-10-23', 1),
       (210, 'Jing', 'user', '2 Av Linton', 2, 'jing@consys.com', '1986-12-26', 1),
       (301, 'Mini', 'user', '3 Av Mason', 0, 'mini@consys.com', '1990-10-25', 1),
       (302, 'Mickey', 'user', '3 Av Mason', 0, 'mickey@consys.com', '1980-12-25', 1),
       (303, 'Steven', 'user', '3 Av Mason', 0, 'steven@consys.com', '1988-11-24', 0),
       (304, 'Bella', 'user', '3 Av Mason', 0, 'bella@consys.com', '1987-11-28', 1),
       (305, 'Cindy', 'user', '3 Av Mason', 0, 'cindy@consys.com', '1993-10-21', 1),
       (306, 'Judith', 'user', '3 Av Mason', 0, 'judith@consys.com', '1995-11-13', 1),
       (307, 'Emma', 'user', '3 Av Mason', 0, 'emma@consys.com', '1994-12-30', 1),
       (308, 'Ellen', 'user', '3 Av Mason', 0, 'ellen@consys.com', '1983-12-12', 1),
       (309, 'Jackson', 'user', '3 Av Mason', 0, 'jackson@consys.com', '1983-10-23', 0),
       (310, 'Sara', 'user', '3 Av Mason', 0, 'sara@consys.com', '1984-12-07', 1),
       (401, 'Messy', 'user', '4 Av Kingston', 0, 'messy@consys.com', '1985-10-29', 1),
       (402, 'Chalk', 'user', '4 Av Kingston', 0, 'chalk@consys.com', '1980-11-19', 1),
       (403, 'Wendy', 'user', '4 Av Kingston', 0, 'wendy@consys.com', '1991-12-29', 1),
       (404, 'Mike', 'user', '4 Av Kingston', 0, 'mike@consys.com', '1989-10-30', 0),
       (405, 'Coco', 'user', '4 Av Kingston', 0, 'coco@consys.com', '1986-12-16', 1),
       (406, 'Tara', 'user', '4 Av Kingston', 0, 'tara@condo.com', '1983-10-30', 1),
       (407, 'Toro', 'user', '4 Av Kingston', 0, 'toro@consys.com', '1990-10-10', 1),
       (408, 'Louis', 'user', '4 Av Kingston', 0, 'louis@consys.com', '1984-12-22', 1),
       (409, 'David', 'user', '4 Av Kingston', 0, 'david@consys.com', '1983-11-24', 1),
       (410, 'Alan', 'user', '4 Av Kingston', 2, 'alan@consys.com', '1988-12-18', 1),
       (501, 'Peanut', 'user', '5 Av Java', 0, 'peanut@consys.com', '1985-12-25', 1),
       (502, 'Lime', 'user', '5 AV Java', 0, 'lime@consys.com', '1991-11-14', 1),
       (503, 'Baby', 'user', '5 AV Java', 0, 'baby@consys.com', '1981-11-19', 0),
       (504, 'Annie', 'user', '5 AV Java', 0, 'annie@consys.com', '1987-12-29', 1),
       (505, 'Delong', 'user', '5 AV Java', 0, 'delong@consys.com', '1984-12-21', 1),
       (506, 'Robert', 'user', '5 AV Java', 0, 'robert@consys.com', '1989-10-19', 0),
       (507, 'Hudson', 'user', '5 AV Java', 0, 'hudson@consys.com', '1983-12-23', 1),
       (508, 'Kitty', 'user', '5 AV Java', 0, 'kitty@consys.com', '1988-12-28', 1),
       (509, 'Kelvin', 'user', '5 AV Java', 0, 'kelvin@consys.com', '1987-11-27', 1),
       (510, 'George', 'user', '5 AV Java', 0, 'george@consys.com', '1990-10-27', 1),
       (601, 'Vincent', 'user', '6 Av Python', 0, 'vincent@consys.com', '1986-12-18', 1),
       (602, 'Hanson', 'user', '6 Av Python', 0, 'hanson@consys.com', '1984-12-22', 1),
       (603, 'Nancy', 'user', '6 Av Python', 0, 'nancy@consys.com', '1986-12-13', 1),
       (604, 'Yvonne', 'user', '6 Av Python', 0, 'yvonne@consys.com', '1993-12-13', 1),
       (605, 'Calvin', 'user', '6 Av Python', 0, 'calvin@consys.com', '1988-12-28', 1),
       (606, 'Stone', 'user', '6 Av Python', 0, 'stone@consys.com', '1986-11-24', 1),
       (607, 'Brown', 'user', '6 Av Python', 0, 'brown@consys.com', '1992-12-20', 1),
       (608, 'Hanna', 'user', '6 Av Python', 0, 'hanna@consys.com', '1987-11-17', 1),
       (609, 'Celin', 'user', '6 Av Python', 0, 'celin@consys.com', '1986-11-26', 1),
       (610, 'Dior', 'user', '6 Av Python', 0, 'dior@consys.com', '1989-10-20', 1);


-- Tables for internal Email system

DROP TABLE IF EXISTS email;
CREATE TABLE email
(
    emailId     INT PRIMARY KEY AUTO_INCREMENT,
    title       VARCHAR(100) NOT NULL,
    content     VARCHAR(400) NOT NULL,
    createdDate DATETIME     NOT NULL,
    isRead      INT          NOT NULL
);

-- value insertion
INSERT INTO email
VALUES (1, 'Salutation', 'Hi, Sam! I am a friend of Sara.', '2020-12-01 16:24:59', 0),
       (2, 'Miss you', 'Hi, Thomas! I miss you so much. Get back to me ASAP.', '2020-12-02 12:33:29', 0),
       (3, 'Say hi', 'Hello, Stephane! I just would like to say hi to you.', '2020-12-03 19:19:23', 0),
       (4, 'Dinner', 'Hi, Ricky. I would like to invite you over for diner this weekend.', '2020-12-03 22:47:49', 0),
       (5, 'Go on vacation', 'We will go on vacation!', '2020-12-04 12:14:40', 0),
       (6, 'Lunch gathering', 'We will have lunch together this weekend.', '2020-12-04 13:24:34', 0),
       (7, 'Moving out', 'We will move by the end of next Month.', '2020-12-04 20:18:38', 0),
       (8, 'Invitation', 'We would like to invite you over', '2020-12-05 14:18:31', 0),
       (9, 'New job', 'I will start a new job tomorrow, so excited!', '2020-12-05 17:45:34', 0),
       (10, 'Parking spot', 'I would like to rent your parking spot.', '2020-12-06 15:41:27', 0),
       (11, 'Farewell party', 'We need to hold a farewell party in two weeks.', '2020-12-09 20:03:29', 0);


DROP TABLE IF EXISTS email_record;
CREATE TABLE email_record
(
    sender_id   INT REFERENCES user (userId),
    receiver_id INT REFERENCES user (userId),
    recordID    INT REFERENCES email (emailId),
    PRIMARY KEY (sender_id, recordID, receiver_id)
);
-- value insertion
INSERT INTO email_record
VALUES (308, 105, 1),
       (101, 304, 2),
       (203, 402, 3),
       (209, 210, 4),
       (307, 401, 5),
       (309, 508, 6),
       (403, 210, 7),
       (505, 609, 8),
       (509, 610, 9),
       (606, 305, 10),
       (109, 601, 11);


DROP TABLE IF EXISTS group_owner;
CREATE TABLE group_owner
(
    groupsID  INT         NOT NULL AUTO_INCREMENT,
    ownerID   INT REFERENCES user (userId),
    groupName VARCHAR(40) NOT NULL,
    PRIMARY KEY (groupsID)
);

-- 10 groups:
-- group1 to group10
-- group_owner,101,102,103,104,105,106,107,108,109,110

INSERT INTO group_owner
VALUES (100, 101, 'Ninja'),
       (101, 102, 'Panda'),
       (102, 103, 'Kungfu'),
       (103, 104, 'Jar'),
       (104, 105, 'Sofa'),
       (105, 106, 'Minibus'),
       (106, 107, 'Vinus'),
       (107, 108, 'Apple'),
       (108, 109, 'NiuNiu'),
       (109, 110, 'Oreo');


DROP TABLE IF EXISTS group_member_list;
CREATE TABLE group_member_list
(
    groupsID INT REFERENCES group_owner (groupsID),
    userID   INT REFERENCES user (userId),
    PRIMARY KEY (groupsID, userID)
);
-- value insertion: no group for 'admin'
INSERT INTO group_member_list
VALUES (100, 101),
       (100, 100),
       (101, 102),
       (102, 103),
       (103, 104),
       (104, 105),
       (105, 106),
       (106, 107),
       (107, 108),
       (108, 109),
       (109, 110),
       (100, 201),
       (101, 202),
       (101, 100),
       (102, 203),
       (103, 204),
       (104, 205),
       (105, 206),
       (106, 207),
       (107, 208),
       (108, 209),
       (109, 210),
       (100, 301),
       (101, 302),
       (102, 303),
       (103, 304),
       (104, 305),
       (105, 306),
       (106, 307),
       (107, 308),
       (108, 309),
       (109, 310);



DROP TABLE IF EXISTS condo_assoc;
CREATE TABLE condo_assoc
(
    associationID   INT    NOT NULL AUTO_INCREMENT,
    assoc_balance   DOUBLE NOT NULL,
    assoc_adminID   INT    NOT NULL REFERENCES user (userId),
    budget          DOUBLE NOT NULL,
    cost_sm_storage DOUBLE NOT NULL,
    cost_sm_parking DOUBLE NOT NULL,
    cost_sm_condo   DOUBLE NOT NULL,
    PRIMARY KEY (associationID, assoc_adminID)
);
-- value insertion: 3 types of CON-Sys 'admins'
-- condo fee (associationID 1 has $2/meter; associationID 2 has $3/meter and associationID 3 has $4/meter)
-- associationID 1 takes care of building 1, associationID 2 takes care of building 2 and 3
-- associationID 3 takes care of building 4, 5 and 6
INSERT INTO condo_assoc
VALUES (1, 10000.00, 110, 100000.00, 2.00, 2.00, 2.00),
       (2, 20000.00, 210, 200000.00, 3.00, 3.00, 3.00),
       (3, 30000.00, 410, 300000.00, 4.00, 4.00, 4.00);


DROP TABLE IF EXISTS assoc_member_list;
CREATE TABLE assoc_member_list
(
    associationID INT REFERENCES condo_assoc (associationID),
    userID        INT REFERENCES user (userId),
    PRIMARY KEY (associationID, userID)
);
-- value insertion
INSERT INTO assoc_member_list
VALUES (1, 100),
       (1, 101),
       (1, 102),
       (1, 103),
       (1, 104),
       (1, 105),
       (1, 106),
       (1, 107),
       (1, 108),
       (1, 109),
       (1, 110),
       (2, 201),
       (2, 202),
       (2, 203),
       (2, 204),
       (2, 205),
       (2, 206),
       (2, 207),
       (2, 208),
       (2, 209),
       (2, 210),
       (2, 301),
       (2, 302),
       (2, 303),
       (2, 304),
       (2, 305),
       (2, 306),
       (2, 307),
       (2, 308),
       (2, 309),
       (2, 310),
       (3, 401),
       (3, 402),
       (3, 403),
       (3, 404),
       (3, 405),
       (3, 406),
       (3, 407),
       (3, 408),
       (3, 409),
       (3, 410),
       (3, 501),
       (3, 502),
       (3, 503),
       (3, 504),
       (3, 505),
       (3, 506),
       (3, 507),
       (3, 508),
       (3, 509),
       (3, 510),
       (3, 601),
       (3, 602),
       (3, 603),
       (3, 604),
       (3, 605),
       (3, 606),
       (3, 607),
       (3, 608),
       (3, 609),
       (3, 610);


DROP TABLE IF EXISTS building;
CREATE TABLE building
(
    buildingID      INT           NOT NULL AUTO_INCREMENT,
    non_public_area DOUBLE(20, 3) NOT NULL,
    public_area     DOUBLE(20, 3) NOT NULL,
    associationID   INT REFERENCES condo_assoc (associationID),
    PRIMARY KEY (buildingID)
);
-- value insertion
INSERT INTO building
VALUES (1, 1030.00, 100.00, 1),
       (2, 700.00, 200.00, 2),
       (3, 700.00, 300.00, 2),
       (4, 800.00, 400.00, 3),
       (5, 800.00, 500.00, 3),
       (6, 900.00, 600.00, 3);


DROP TABLE IF EXISTS condo_unit;
CREATE TABLE condo_unit
(
    condoID          INT          NOT NULL AUTO_INCREMENT,
    ownerID          INT,
    buildingID       INT REFERENCES building (buildingID),
    size             DOUBLE(7, 3) NOT NULL,
    percentage_share DOUBLE(7, 6) NOT NULL,
    current_fee      DOUBLE       NOT NULL,
    PRIMARY KEY (condoID)
);
-- value insertion: user101 has 2 condos and user102 has 3 condos
INSERT INTO condo_unit
VALUES (1, 100, 1, 100.00, 0.117647, 200.00),
       (2, 101, 1, 40.00, 0.047058, 80.00),
       (3, 101, 1, 40.00, 0.047058, 80.00),
       (4, 102, 1, 50.00, 0.058823, 100.00),
       (5, 102, 1, 50.00, 0.058823, 100.00),
       (6, 102, 1, 50.00, 0.058823, 100.00),
       (7, 103, 1, 50.00, 0.058823, 100.00),
       (8, 104, 1, 50.00, 0.058823, 100.00),
       (9, 105, 1, 60.00, 0.070588, 120.00),
       (10, 106, 1, 60.00, 0.070588, 120.00),
       (11, 107, 1, 70.00, 0.082352, 140.00),
       (12, 108, 1, 70.00, 0.082352, 150.00),
       (13, 109, 1, 80.00, 0.094117, 170.00),
       (14, 110, 1, 80.00, 0.094117, 140.00),
       (15, 201, 2, 50.00, 0.071428, 150.00),
       (16, 202, 2, 50.00, 0.071428, 150.00),
       (17, 203, 2, 60.00, 0.085714, 180.00),
       (18, 204, 2, 60.00, 0.085714, 180.00),
       (19, 205, 2, 70.00, 0.100000, 210.00),
       (20, 206, 2, 70.00, 0.100000, 210.00),
       (21, 207, 2, 80.00, 0.114285, 240.00),
       (22, 208, 2, 80.00, 0.114285, 255.00),
       (23, 209, 2, 90.00, 0.128571, 285.00),
       (24, 210, 2, 90.00, 0.128571, 330.00),
       (25, 301, 3, 45.00, 0.064285, 150.00),
       (26, 302, 3, 55.00, 0.078571, 150.00),
       (27, 303, 3, 60.00, 0.085714, 180.00),
       (28, 304, 3, 60.00, 0.085714, 180.00),
       (29, 305, 3, 70.00, 0.100000, 210.00),
       (30, 306, 3, 70.00, 0.100000, 210.00),
       (31, 307, 3, 80.00, 0.114285, 240.00),
       (32, 308, 3, 80.00, 0.114285, 255.00),
       (33, 309, 3, 90.00, 0.128571, 285.00),
       (34, 310, 3, 90.00, 0.128571, 330.00),
       (35, 401, 4, 60.00, 0.075000, 240.00),
       (36, 402, 4, 60.00, 0.075000, 240.00),
       (37, 403, 4, 70.00, 0.087500, 280.00),
       (38, 404, 4, 70.00, 0.087500, 280.00),
       (39, 405, 4, 80.00, 0.100000, 320.00),
       (40, 406, 4, 80.00, 0.100000, 320.00),
       (41, 407, 4, 90.00, 0.112500, 360.00),
       (42, 408, 4, 90.00, 0.112500, 380.00),
       (43, 409, 4, 100.00, 0.125000, 420.00),
       (44, 410, 4, 100.00, 0.1250001, 480.00),
       (45, 501, 5, 60.00, 0.075000, 240.00),
       (46, 502, 5, 60.00, 0.075000, 240.00),
       (47, 503, 5, 70.00, 0.087500, 280.00),
       (48, 504, 5, 70.00, 0.087500, 280.00),
       (49, 505, 5, 80.00, 0.100000, 320.00),
       (50, 506, 5, 80.00, 0.100000, 320.00),
       (51, 507, 5, 90.00, 0.112500, 360.00),
       (52, 508, 5, 90.00, 0.112500, 380.00),
       (53, 509, 5, 100.00, 0.125000, 420.00),
       (54, 510, 5, 100.00, 0.1250001, 480.00),
       (55, 601, 6, 70.00, 0.077777, 280.00),
       (56, 602, 6, 70.00, 0.077777, 280.00),
       (57, 603, 6, 80.00, 0.088888, 320.00),
       (58, 604, 6, 80.00, 0.088888, 320.00),
       (59, 605, 6, 90.00, 0.100000, 360.00),
       (60, 606, 6, 90.00, 0.100000, 360.00),
       (61, 607, 6, 100.00, 0.111111, 400.00),
       (62, 608, 6, 100.00, 0.111111, 420.00),
       (63, 609, 6, 110.00, 0.122222, 460.00),
       (64, 610, 6, 110.00, 0.122222, 520.00),
       (65, null, 6, 160.00, 0.122222, 520.00),
       (66, null, 6, 110.00, 0.122222, 520.00),
       (67, null, 6, 110.00, 0.122222, 520.00),
       (68, null, 6, 110.00, 0.122222, 520.00),
       (69, null, 1, 89.00, 0.122222, 520.00),
       (70, null, 1, 110.00, 0.122222, 220.00),
       (71, null, 1, 110.00, 0.122222, 420.00),
       (72, null, 2, 120.00, 0.122222, 560.00),
       (73, null, 2, 120.00, 0.122222, 530.00),
       (74, null, 2, 120.00, 0.122222, 560.00),
       (75, null, 3, 120.00, 0.122222, 510.00),
       (76, null, 3, 180.00, 0.122222, 220.00),
       (77, null, 3, 150.00, 0.122222, 520.00),
       (78, null, 4, 150.00, 0.122222, 520.00),
       (79, null, 4, 120.00, 0.122222, 420.00),
       (80, null, 4, 210.00, 0.19, 620.00),
       (81, null, 5, 210.00, 0.22, 620.00),
       (82, null, 5, 210.00, 0.19, 590.00),
       (83, null, 5, 210.00, 0.22, 640.00);


DROP TABLE IF EXISTS parking;
CREATE TABLE parking
(
    parkingID INT    NOT NULL AUTO_INCREMENT,
    condoID   INT REFERENCES condo_unit (condoID),
    size      DOUBLE NOT NULL,
    PRIMARY KEY (parkingID)
);
-- value insertion: parking size is 27
INSERT INTO parking
VALUES (1, 1, 27.00),
       (2, 2, 27.00),
       (3, 2, 27.00),
       (4, 3, 27.00),
       (5, 4, 27.00),
       (6, 4, 27.00),
       (7, 5, 27.00),
       (8, 6, 27.00),
       (9, 6, 27.00),
       (10, 7, 27.00),
       (11, 8, 27.00),
       (12, 8, 27.00),
       (13, 9, 27.00),
       (14, 10, 27.00),
       (15, 10, 27.00),
       (16, 11, 27.00),
       (17, 12, 27.00),
       (18, 12, 27.00);


DROP TABLE IF EXISTS storage;
CREATE TABLE storage
(
    storageID INT    NOT NULL AUTO_INCREMENT,
    condoID   INT REFERENCES condo_unit (condoID),
    size      DOUBLE NOT NULL,
    PRIMARY KEY (storageID)
);
-- value insertion: storage size is: 11 (condoID '1' has 2 storage, condoID '6' has 2 storage, condoID '12' has 2 storage)
INSERT INTO storage
VALUES (1, 1, 11.00),
       (2, 1, 11.00),
       (3, 2, 11.00),
       (4, 3, 11.00),
       (5, 4, 11.00),
       (6, 5, 11.00),
       (7, 6, 11.00),
       (8, 6, 11.00),
       (9, 7, 11.00),
       (10, 8, 11.00),
       (11, 9, 11.00),
       (12, 10, 11.00),
       (13, 11, 11.00),
       (14, 12, 11.00),
       (15, 12, 11.00),
       (16, 13, 11.00),
       (17, 14, 11.00),
       (18, 15, 11.00),
       (19, 16, 11.00),
       (20, 17, 11.00),
       (21, 18, 11.00),
       (22, 19, 11.00);


DROP TABLE IF EXISTS financial_record;
CREATE TABLE financial_record
(
    feeID            INT         NOT NULL AUTO_INCREMENT,
    associationID    INT REFERENCES condo_assoc (associationID),
    fee              DOUBLE      NOT NULL,
    contractor_payer VARCHAR(40) NOT NULL,
    type             varchar(20) NOT NULL,
    date_of_payment  DATE,
    PRIMARY KEY (feeID, associationID)
);
-- value insertion
INSERT INTO financial_record
VALUES (1, 3, 1999.99, 'ABC company', 'Outcome', '2020-11-23'),
       (2, 3, 976.03, 'Delicious company', 'Outcome', '2020-11-29'),
       (3, 3, 456.95, 'Happy company', 'Outcome', '2020-12-3'),
       (4, 3, 334.50, 'user401', 'Income', '2020-12-4');

-- trigger creation
DELIMITER $$
CREATE TRIGGER tri_1
    AFTER INSERT
    ON financial_record
    FOR EACH ROW
BEGIN
    IF (new.type = 'Outcome') THEN
        UPDATE condo_assoc
        SET assoc_balance=assoc_balance - new.fee
        WHERE associationID = new.associationID;
    END IF;

    IF (new.type = 'Income') THEN
        UPDATE condo_assoc
        SET assoc_balance=assoc_balance + new.fee
        WHERE associationID = new.associationID;
    END IF;

END$$
DELIMITER ;

-- value insertion
INSERT INTO financial_record
VALUES (5, 1, 220.00, 'user107', 'Income', '2020-12-1'),
       (6, 2, 315.00, 'user202', 'Income', '2020-12-1'),
       (7, 2, 200.00, 'user206', 'Income', '2020-12-1'),
       (8, 2, 405.00, 'Painting Company', 'Outcome', '2020-12-2'),
       (9, 1, 202.00, 'user106', 'Income', '2020-12-2'),
       (10, 1, 310.00, 'Garage Collection Company', 'Outcome', '2020-12-3'),
       (11, 1, 1750.00, 'Cleaning Company', 'Outcome', '2020-12-3'),
       (12, 2, 208.00, 'user202', 'Income', '2020-12-4'),
       (13, 2, 240.00, 'Grass Company', 'Outcome', '2020-12-4'),
       (14, 1, 450.00, 'user107', 'Income', '2020-12-4'),
       (15, 3, 230.00, 'user502', 'Income', '2020-12-4'),
       (16, 3, 279.00, 'user503', 'Income', '2020-12-4'),
       (17, 2, 130.00, 'Snow Company', 'Outcome', '2020-12-4'),
       (18, 3, 670.00, 'user607', 'Income', '2020-12-5'),
       (19, 3, 440.00, 'user602', 'Income', '2020-12-6'),
       (20, 3, 120.00, 'user603', 'Income', '2020-12-7');

DELIMITER $$
CREATE TRIGGER nofees_tri_con
    BEFORE INSERT
    ON financial_record
    FOR EACH ROW
BEGIN
    IF (new.type = 'Outcome') THEN
        SELECT assoc_balance FROM condo_assoc WHERE associationID = new.associationID INTO @restfee;
        IF @restfee < new.fee THEN
            INSERT INTO DONT_HAVE_ENOUGH_FEES VALUES ('Not enough balance');
        END IF;
    END IF;

END$$
DELIMITER ;


DROP TABLE IF EXISTS post;
CREATE TABLE post
(
    post_ID          INT          NOT NULL AUTO_INCREMENT,
    last_modified_at DATETIME     NOT NULL,
    posting_time     DATETIME     NOT NULL,
    content          TEXT         NOT NULL,
    ownerID          INT REFERENCES user (userId),
    parent_postID    INT,
    permit_rule      INT          NOT NULL,
    posting_type     VARCHAR(30)  NOT NULL,
    title            VARCHAR(100) NOT NULL,
    PRIMARY KEY (post_ID)
);
-- value insertion
# Permit rule: -1 --> close/private , 0 --> public, 1,2,3 --> associate, 100+ --> group ids
INSERT INTO post
VALUES (1, '2020-07-04 15:28:58', '2020-07-04 15:27:49', 'We will hold a meeting this Saturday night at 17h00.', 100,
        NULL, 0, 'meeting', 'Weekly meeting'),
       (2, '2020-07-05 20:14:34', '2020-07-05 20:13:59', 'Others cars can not pass through!', 101, 1, 0, 'comment',
        'Please do not park in the drive way'),
       (3, '2020-07-06 10:35:05', '2020-07-06 10:33:57',
        'Smoking at the front door bothers other people who do not smoke, please step further to smoke. Thank you very much for your cooperation and understanding !',
        102, 1, 0, 'comment', 'Please stop smoking at the front door'),
       (4, '2020-07-07 17:13:03', '2020-07-07 17:12:53',
        'Please fill out the requested owner registration forms by the end of this week.', 102, NULL, 0, 'general',
        'Registration form'),
       (5, '2020-07-08 13:28:43', '2020-07-08 13:28:40', 'Saturday evenings are the best time for us to meet up.', 202,
        4, 0, 'comment', 'Member meeting time'),
       (6, '2020-07-09 18:33:13', '2020-07-09 18:33:11',
        'Would like to have more parking spaces available for visitors!', 302, 4, 0, 'comment', 'Visitor parking spot'),
       (7, '2020-07-10 14:48:43', '2020-07-10 13:48:43', 'A 2018 VW Tiguan for sale with mint condition.', 301, NULL, 0,
        'ads', 'Pre-owned VW for sale'),
       (8, '2020-07-11 23:14:23', '2020-07-11 23:14:20', 'Is your dinning table big enough for 6 people.', 401, 7, 0,
        'comment', 'Interested in buying a used dinning table'),
       (9, '2020-07-12 15:22:40', '2020-07-12 15:22:38', 'From where to where and when?', 501, 7, 0, 'comment',
        'Asking for a ride'),
       (10, '2020-07-13 16:56:13', '2020-07-13 16:56:10', 'Do we need some Halloween decorations?', 201, NULL, 2,
        'vote', 'Halloween decorations'),
       (11, '2020-07-14 11:10:22', '2020-07-14 11:09:59', 'Please do not leave your snow boots outside!', 202, 10, 2,
        'comment', 'Snow boots'),
       (12, '2020-07-15 18:34:39', '2020-07-15 18:33:57',
        'I do not want to pay for the snow removal service this year.', 203, 10, 2, 'comment',
        'I want a snow removal service'),
       (13, '2020-07-16 11:19:48', '2020-07-16 11:19:45',
        'For those who stay in Montreal during Christmas season, we can do something fun together!', 204, 10, 2,
        'comment', 'We can have a Christmas party'),
       (14, '2020-07-16 12:11:11', '2020-07-16 12:11:10',
        'Please remember to attend out monthly meeting this Saturday night. Thank you all very much!', 103, NULL, 0,
        'meeting', 'Meeting reminder'),
       (15, '2020-07-17 19:50:19', '2020-07-17 19:49:56', 'We both would like to come join you all.', 108, 14, 0,
        'comment', 'Who will come to the dinner gathering'),
       (16, '2020-07-18 22:19:19', '2020-07-18 22:19:13', 'I would like to come and help.', 303, 14, 0, 'comment',
        'Old furniture donation'),
       (17, '2020-09-18 23:30:31', '2020-09-18 23:30:25',
        'We will get a raise on the condo fee! Please vote your voices!', 101, NULL, 100, 'vote', 'Condo fee raise'),
       (18, '2020-09-22 10:19:40', '2020-09-22 10:19:23', 'Please improve out garage collection efficiency!', 407, 17,
        3, 'comment', 'Garage collection'),
       (19, '2020-10-15 23:38:44', '2020-10-15 23:38:40', 'How should I contact you?', 100, NULL, 1, 'ads',
        'Old clothes for sale'),
       (20, '2020-11-19 15:37:11', '2020-11-19 15:37:10', 'I have a cello for sale, please contact at 514-666888.', 609,
        NULL, 3, 'ads', 'Cello for sell'),
       (21, '2020-11-25 23:49:33', '2020-11-25 23:49:32', 'How much does it cost?', 503, NULL, -1, 'contract',
        'Sink reparation'),
       (22, '2020-11-26 10:12:57', '2020-11-26 10:12:57', 'What kind of services do you need?', 302, NULL, -1,
        'contract', 'Need a contractor'),
       (23, '2020-12-05 23:18:23', '2020-12-05 23:18:20', 'How can I contact you?', 110, NULL, -1, 'contract',
        'Need some help in house cleaning'),
       (24, '2020-12-15 23:18:23', '2020-12-03 23:18:20', 'How can I contact you?', 110, NULL, 109, 'meeting',
        'Group meeting'),
       (25, '2020-12-15 23:18:23', '2020-12-06 23:18:20', 'How can I contact you?', 210, NULL, 2, 'meeting',
        'Association 2 meeting'),
       (26, '2020-12-15 23:18:23', '2020-12-07 21:18:20', 'How can I contact you?', 410, NULL, 3, 'meeting',
        'Association 3 meeting'),
       (27, '2020-12-15 23:18:23', '2020-12-08 21:18:20', 'How can I contact you?', 100, NULL, 0, 'general',
        'What is the time'),
       (28, '2020-12-15 23:18:23', '2020-12-09 21:18:20', 'How can I contact you?', 210, NULL, 0, 'general',
        'Winter is coming'),
       (29, '2020-12-15 23:18:23', '2020-12-06 20:18:20', 'How can I contact you?', 110, NULL, 0, 'general',
        'There will be a strong snow'),
       (30, '2020-12-15 23:18:23', '2020-12-06 21:17:20', 'How can I contact you?', 100, NULL, 100, 'general',
        'Where can I buy ice cream'),
       (31, '2020-12-15 23:18:23', '2020-12-06 21:14:20', 'How can I contact you?', 107, NULL, 1, 'general',
        'Go hiking');

-- trigger creation
DELIMITER $$
CREATE TRIGGER post_delete_tri
    BEFORE DELETE
    ON post
    FOR EACH ROW
BEGIN
    IF (old.posting_type = 'ads') THEN
        DELETE ads.* FROM ads WHERE Post_ID = old.post_ID;
    END IF;

    IF (old.posting_type = 'meeting') THEN
        DELETE meeting.* FROM meeting WHERE post_ID = old.post_ID;
        DELETE join_meeting.* FROM join_meeting WHERE post_ID = old.post_ID;
    END IF;

    IF (old.posting_type = 'vote') THEN
        DELETE vote.* FROM vote WHERE post_ID = old.post_ID;
        DELETE join_vote.*FROM join_vote WHERE post_ID = old.post_ID;
    END IF;

END$$
DELIMITER ;

DROP TABLE IF EXISTS ads;
CREATE TABLE ads
(
    post_ID    INT REFERENCES post (post_ID),
    ads_status VARCHAR(50) NOT NULL,
    PRIMARY KEY (post_ID)
);
-- value insertion
INSERT INTO ads
VALUES (7, 'unsold'),
       (19, 'unsold'),
       (20, 'unsold');

DROP TABLE IF EXISTS vote;
CREATE TABLE vote
(
    post_ID INT REFERENCES post (post_ID),
    agreed     INT,
    disagreed     INT,
    PRIMARY KEY (post_ID)
);
-- value insertion
INSERT INTO vote
VALUES (10, 2, 2),
       (17, 0, 0);



DROP TABLE IF EXISTS vote_list;
CREATE TABLE vote_list
(
    userID  INT REFERENCES user (userId),
    post_ID INT REFERENCES post (post_ID),
    isVote INT,

    PRIMARY KEY (userID, post_ID)
);
-- value insertion   isvote: 0: not yet, 1: agreed, 2:disagreed
INSERT INTO vote_list
VALUES (201, 10, 1),
       (202, 10, 2),
       (203, 10, 1),
       (204, 10, 0),
       (205, 10, 0),
       (206, 10, 0),
       (207, 10, 0),
       (208, 10, 0),
       (209, 10, 0),
       (210, 10, 0),
       (301, 10, 2),
       (302, 10, 0),
       (303, 10, 0),
       (304, 10, 0),
       (305, 10, 0),
       (306, 10, 0),
       (307, 10, 0),
       (308, 10, 0),
       (309, 10, 0),
       (310, 10, 0),
       (100, 17, 0),
       (101, 17, 0),
       (201, 17, 0),
       (301, 17, 0);


DROP TABLE IF EXISTS meeting;
CREATE TABLE meeting
(
    pid   INT REFERENCES post (post_ID),
    start DATETIME NOT NULL,
    end   DATETIME NOT NULL,
    PRIMARY KEY (pid)
);
-- value insertion
INSERT INTO meeting
VALUES (1, '2020-07-11 18:30:00', '2020-07-11 19:00:00'),
       (14, '2020-07-22 18:00:00', '2020-07-22 18:50:00'),
       (24, '2020-12-22 18:00:00', '2020-12-22 18:50:00'),
       (25, '2020-12-25 13:00:00', '2020-12-25 13:30:00'),
       (26, '2020-12-26 18:00:00', '2020-12-26 18:50:00');


