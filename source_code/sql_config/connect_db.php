<?php
/*
Developers:
Sasa Zhang 25117151
Ruoshi Wu 27448724
Jinchen Hu 40080398

*/
require_once $_SERVER['DOCUMENT_ROOT'].'/config.php';
// Establish database connection
$conn = new mysqli(SERVER_NAME, DB_USER, DB_PASSWORD, DB_NAME);
// Check connected successfully or not
if ($conn->connect_error) {
    die("Connection has failed: " . $conn->connect_error);
}

