<?php
/*
Home page
*/

include 'partials/check_login.php';
include 'partials/header.php';
include 'sql_config/connect_db.php';

?>

    <div class="container">
        <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active img-fluid">
                    <img src="images/city1.jpg" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>1777 Rue du Fort</h5>
                        <p>Our first and the most famous condo</p>
                    </div>
                </div>
                <div class="carousel-item img-fluid">
                    <img src="images/city2.jpg" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>8766 Westhell Treystanter avenue</h5>
                        <p>The newest condo with the highest facilities</p>
                    </div>
                </div>
                <div class="carousel-item img-fluid">
                    <img src="images/city3.jpg" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>2612 Chong Qing Road</h5>
                        <p>High-end condo, ecosystem fitment</p>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <br>
    <br>
    <hr class="my-4">
    <div class="container">
        <div class="card-deck">
                <?php

                if(isset($_SESSION['loggedin'])){
                    $goto = "posting/post_list.php";
                }else{
                    $goto = "partials/login.php";
                }

                // Select the most current 3 postings
                $post_result = $conn->query("SELECT * FROM post where permit_rule = 3 ORDER BY last_modified_at DESC LIMIT 3");
                $num = 1;
                if(!is_null($post_result) && $post_result->num_rows > 0){
                    while($row = $post_result->fetch_assoc()){
                        $title = $row['title'];
                        $content = $row['content'];
                        $time = $row['last_modified_at'];
                        echo '<div class="card">';
                        echo '<img src="images/ad'.$num.'.jpg" class="card-img-top" alt="...">';
                        $num += 1;

                        echo '<a href="'.$goto.'"><div class="card-body">';
                        echo '<h5 class="card-title">'.$title.'</h5>';
                        echo '<p class="card-text">'.$content.'</p></div>';
                        echo '<div class="card-footer">
                                <small class="text-muted"> Last update: '.$time.'</small>
                                </div>
                                </a></div>';
                    }
                }else{
                    echo '<div class="alert alert-danger" role="alert">Sorry, no News for public currently</div>';
                }

                ?>
        </div>
    </div>
<?php
include 'partials/footer.php';
?>