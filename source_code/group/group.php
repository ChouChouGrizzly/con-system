<?php
/*
Financial records  page
*/
/*
Developers:
Sasa Zhang 25117151
Ruoshi Wu 27448724
Jinchen Hu 40080398

*/
include '../partials/check_login.php';
include '../partials/header.php';
include '../sql_config/connect_db.php';

$userid = $_SESSION['userid'];


//find your groups from group_list
$sql_groups = "SELECT * from group_member_list where userID = '$userid' ";
//execute query
$result_groups = $conn->query($sql_groups);
//num of result
$num = $result_groups->num_rows;
//check if user is exist
if ($num > 0) {
    //$row = mysqli_fetch_array($result);

} else {
//    displayWarning( $num . "Error: find groups function " . $sql_groups . "<br>" . mysqli_error($conn));
    displayWarning( "No group Info presented for this user");
}

//read
if (isset($_GET['operator']) && isset($_GET['record']) && $_GET['operator'] == 1) {
    //get record id
    $re_id = $_GET['record'];
    //change unread status
    $sql_read = "update email_collected set isRead = 1 where emailId = '$re_id'";
    if (mysqli_query($conn, $sql_read)) {
        //unread success
        //header("location: messages.php?ID=$click_receive");
        //echo 'read success';
    } else {
        displayError( "Error: set read status function " . $sql_read . "<br>" . mysqli_error($conn));
    }
}
//delete message
if (isset($_GET['operator']) && isset($_GET['record']) && $_GET['operator'] == 3) {
    //get record id
    $re_id = $_GET['record'];
    //change unread status
    $sql_delete_1 = "delete from email_sendto where recordID = '$re_id'";

    if (mysqli_query($conn, $sql_delete_1)) {
        //echo 'delete success';
        //then delete email_recorded
        $sql_delete_2 = "delete from email_sendto where recordID = '$re_id'";
        if (mysqli_query($conn, $sql_delete_2)) {
            //echo 'delete success';
        } else {
            displayError( "Error: set delete message function 2 " . $sql_delete_2 . "<br>" . mysqli_error($conn));

        }
    } else {
        displayError( "Error: set delete message function 1 " . $sql_delete_1 . "<br>" . mysqli_error($conn));
    }
}

//check refresh button
if (isset($_GET['operator']) && $_GET['operator'] == 'refresh') {
    header("location:groups.php");
    exit;
}

//check create new group
//set create_group_status=0
$create_group_status = 0;
if (isset($_GET['operator']) && $_GET['operator'] == 'create_new_group') {
    $create_group_status = 1;
}

//check new group form
$new_group_status = 0;
//check if send message
if (isset($_POST["submit"]) && $_POST["submit"] == "create") {
    $new_group_info = $_POST['group_info'];
    //
    //find the latest groupsID, then add 1 for new_group_id
    $sql_m = "select max(groupsID) from group_owner ";
    $result = $conn->query($sql_m);
    //check if the email_recorded is empty
    $num_empty = $result->num_rows;
    if ($num_empty) {
        $row = mysqli_fetch_array($result);
        $new_group_id = $row[0] + 1;
    } else {
        //initial message_id
        $new_group_id = 10000;
    }
    //insert new group
    $sql_new_group = "insert into group_owner values('$new_group_id','$userid','$new_group_info')";
    //execute
    if (mysqli_query($conn, $sql_new_group)) {
        //then, insert into group_list
        $sql_new_group_list = "insert into group_member_list values('$new_group_id','$userid')";
        if (mysqli_query($conn, $sql_new_group_list)) {
            //set status
            $new_group_status = 1;
        } else {
            displayError( "Error: insert new group list " . $sql_new_group . "<br>" . mysqli_error($conn));
        }

    } else {
        displayError("Error: create new group " . $sql_new_group . "<br>" . mysqli_error($conn));
    }
}
//set del_group_status
$del_group_status = 0;
//check delete button
if (isset($_GET['operator']) && $_GET['operator'] == 'delete' && isset($_GET['group_id'])) {
    //delete the group
    $delete_group_id = $_GET['group_id'];
    //first delete from group_list
    $sql_del_list = "delete from group_member_list where groupsID=$delete_group_id";
    if (mysqli_query($conn, $sql_del_list)) {
        //then, delete from group_leader
        $sql_del_leader = "delete from group_owner where groupsID=$delete_group_id";
        if (mysqli_query($conn, $sql_del_leader)) {
            //success
            $del_group_status = 1;
            header("location:groups.php");
        } else {
            displayError( "Error: delete group leader " . $sql_del_list . "<br>" . mysqli_error($conn));
        }
    } else {
        displayError( "Error: delete group list " . $sql_del_list . "<br>" . mysqli_error($conn));
    }
}
//check exit group
if (isset($_GET['operator']) && $_GET['operator'] == 'exit_group' && isset($_GET['group_id'])) {
    //delete the group
    $exit_group_id = $_GET['group_id'];
    //first delete from group_list
    $sql_exit_list = "delete from group_member_list where groupsID='$exit_group_id' and userID = '$userid'";
    if (mysqli_query($conn, $sql_exit_list)) {
        header("location:groups.php");
    } else {
        diaplsyerror( "Error: exit group list " . $sql_exit_list . "<br>" . mysqli_error($conn));
    }
}

//check delete member of group
$del_member_status = 0;
if (isset($_GET['operator']) && $_GET['operator'] == 'delete_member' && isset($_GET['group_id']) && isset($_GET['del_user_id'])) {
    $del_user_group = $_GET['group_id'];
    $del_user_id = $_GET['del_user_id'];
    //delete from group list
    $sql_del_user = "delete from group_member_list where groupsID='$del_user_group' and userID = '$del_user_id' ";
    //execute query
    if (mysqli_query($conn, $sql_del_user)) {
        $del_member_status = 1;
    } else {
        displayError( "Error: delete member of group " . $sql_del_user . "<br>" . mysqli_error($conn));
    }
}

//check manage groups
$manage_status = 0;
if (isset($_GET['operator']) && $_GET['operator'] == 'manage' && isset($_GET['group_id'])) {
    $manage_group_id = $_GET['group_id'];
    //set session
    $_SESSION['manage_group_id'] = $manage_group_id;
    //search group list
    $sql_manage = "select userID from group_member_list where groupsID='$manage_group_id' and userID != '$userid' ";
    //execute query
    $result_manage = $conn->query($sql_manage);
    $num = $result_manage->num_rows;
    if ($num) {
        //set status
        $manage_status = 1;
    } else {
        //echo "Error: manage group " . $sql_manage . "<br>" . mysqli_error($conn);
        //set error status
        $manage_status = 3;
    }
}
//add member
$add_member_status = 0;
if (isset($_POST['submit']) && $_POST['submit'] == 'add_member') {
    //get group id
    $add_group_id = $_SESSION['manage_group_id'];
    $add_user_id = $_POST['add_member_id'];
    //sql add member
    $sql_add_member = "insert into group_member_list values('$add_group_id', '$add_user_id')";
    //execute
    if (mysqli_query($conn, $sql_add_member)) {
        $add_member_status = 1;
    } else {
        displayError( "Error: add member " . $sql_add_member . "<br>" . mysqli_error($conn));
    }
}

//modify group info
$modify_info_status = 0;
if (isset($_POST['submit']) && $_POST['submit'] == 'modify_group_info') {
    //get group id
    $modify_group_id = $_SESSION['manage_group_id'];
    $modify_group_info = $_POST['modify_group_info'];
    //sql update
    $sql_modify_group_info = "update group_owner set groupName='$modify_group_info' where groupsID='$modify_group_id' ";
    //execute
    if (mysqli_query($conn, $sql_modify_group_info)) {
        $modify_info_status = 1;
    } else {
        displayError( "Error: modify_group_info " . $sql_modify_group_info . "<br>" . mysqli_error($conn));
    }

}

//apply for group
$apply_group_status = 0;
if (isset($_POST['submit']) && $_POST['submit'] == 'apply_group') {
    //get group id, user if
    $apply_group_Name = $_POST['apply_group'];
    $apply_user_id = $userid;
    //find ownerID, then send a message which status is 7 for apply for group
    $sql_owner = "select ownerID from group_owner where groupName='$apply_group_Name'";
    //find group Name

    //execute
    $result_owner = $conn->query($sql_owner);
    $num_owner = $result_owner->num_rows;
    if ($num_owner) {
        //get owner ID
        $row_owner = mysqli_fetch_array($result_owner);
        $send_owner_id = $row_owner['ownerID'];
        //find the latest message_id, then add 1 for new_message_id
        $sql_m = "select max(emailId) from email";
        $result = $conn->query($sql_m);
        //check if the email_recorded is empty
        $num_empty = $result->num_rows;
        if ($num_empty) {
            $row = mysqli_fetch_array($result);
            $message_id = $row[0] + 1;
        } else {
            //initial message_id
            $message_id = 10000;
        }
        //get current time
        $time = date('Y-m-d H:i:s');
        //send messgae to ownerID
        $msg_apply = '[SYSTEM]:apply_for_group' . $apply_group_id;
        $sql_msg_owner = "insert into email values('$message_id','$msg_apply', '$msg_apply',  '$time', '0') ";
        if (mysqli_query($conn, $sql_msg_owner)) {
            //then insert info to email_sharedby
            $sql_sharedby = "insert into email_record values ('$apply_user_id', '$send_owner_id', '$message_id')";
            if (mysqli_query($conn, $sql_sharedby)) {
                $apply_group_status = 1;
            } else {
                displayError( "Error: send msg to ownerID " . $sql_sharedby . "<br>" . mysqli_error($conn));
            }

        } else {
            displayError( "Error: send msg to ownerID " . $sql_msg_owner . "<br>" . mysqli_error($conn));
        }
    } else {
        displayError( "Error: No found Owner in Group_leader " . $sql_owner . "<br>" . mysqli_error($conn));
    }

}

?>
<?php

//manage_status error
if ($manage_status == 3) {
//    echo '
//    <div class="alert alert-danger" role="alert">Failed to delete or insert</div>';
    displayError("Failed to delete or insert");
}


if ($del_member_status == 1) {
//    echo '
//    <div class="alert alert-success" role="alert">Successfully delete the member</div>';
    displaySuccess("Successfully deleted the member");
}

if ($add_member_status == 1) {
//    echo '
//    <div class="alert alert-success" role="alert">Successfully add the member</div>';
    displaySuccess("Successfully added the member");
}
if ($modify_info_status == 1) {
//    echo '
//    <div class="alert alert-success" role="alert">Successfully change the group name</div>';
    displaySuccess("Successfully changed the group name");
}
if ($apply_group_status == 1) {
//    echo '
//    <div class="alert alert-success" role="alert">You will be added once the owner approved</div>';
    displaySuccess("You will be added once the owner approved");
}if ($new_group_status == 1) {
//    echo "Create new group successful!";
    displaySuccess("Successfully created a new group");
}
if ($del_group_status == 1) {
//    echo '
//    <div class="alert alert-success" role="alert">Successfully delete the group</div>';
    displaySuccess("Successfully deleted the group");
}

?>

    <div class="container">
        <div class="card">
            <div class="card-header">
                <h4 class="mb-0">Manage Group</h4>
            </div>
            <div class="card-body">

                <?php



                if ($manage_status == 1) {

                    echo "<h4 style='align-content: center'>GROUP - " . $_SESSION['manage_group_id'] . "</h4>";
                    echo '<form action=""  method="post">
                        <div class="input-group input-group-sm">
                       <input type="text" class="form-control" id="add_member_id" name="add_member_id" placeholder="please enter userID "/>
                        <div class="input-group-btn">&nbsp;
                       <button type="submit" class="btn btn-primary btn-sm" name="submit" value="add_member">add member</button>
                     </div> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                       <input type="text" class="form-control" id="modify_group_info" name="modify_group_info" placeholder="please enter new information... "/>
                     <div class="input-group-btn">&nbsp;
                       <button type="submit" class="btn btn-primary btn-sm" name="submit" value="modify_group_info">Change Group Name</button>
                     </div>
                     </div>
                     </form><br>';

                    echo '
                <table class="table table-striped table-hover table-bordered"> ';
                    echo '
                <tr>
                <th style="align-content: center">UserID</th>
                <th style="align-content: center">Delete</th>
                </tr>
                ';
                    while ($row_manage = mysqli_fetch_array($result_manage)) {
                        $manage_user_id = $row_manage['userID'];
                        $manage_group_id = $_SESSION['manage_group_id'];
                        echo "<tr><td>" . $manage_user_id . "</td><td>" . "<a href='group.php?operator=delete_member&group_id=$manage_group_id&del_user_id=$manage_user_id' > 
                                  <button type='button' class='btn btn-danger btn-sm' name='delete_member'>delete member </button>
                                </a> " . "</td></tr>";

                    }//while

                    echo '</table><hr class="my-4">';
                }


                ?>

                <div class="container-fluid yo">
                    <br>
                    <table class="table table-hover table-striped ">
                        <tr>
                            <th style="align-content: center">GroupID</th>
                            <th style="align-content: center">Information</th>
                            <th style="align-content: center">OwnerName</th>
                            <th style="align-content: center">Status</th>
                            <th style="align-content: center">Manage</th>
                            <th style="align-content: center">Operator</th>

                        </tr>
                        <?php
                        //find your all groups from group_leader
                        while ($row = mysqli_fetch_array($result_groups)) {
                            //find each group info
                            $get_group_id = $row['groupsID'];
                            $sql_group_info = "select groupName, ownerID from group_owner where groupsID='$get_group_id' ";
                            //execute sql
                            $result_group_info = $conn->query($sql_group_info);
                            //num of result
                            $num_group_info = $result_group_info->num_rows;
                            //check num
                            if ($num_group_info) {
                                //output info
                                while ($row_group_info = mysqli_fetch_array($result_group_info)) {
                                    $get_group_info = $row_group_info['groupName'];
                                    $get_owner_id = $row_group_info['ownerID'];
                                    $checkOwner=mysqli_query($conn,"SELECT * FROM user WHERE userId='$get_owner_id'");
                                    $owner=mysqli_fetch_array($checkOwner);
                                    $ownerName=$owner['userName'];

                                    //change owner status
                                    if ($userid == $get_owner_id) {
                                        $owner_status = 'owner';
                                        //echo table
                                        echo "<tr><td>" . $get_group_id . "</td><td>" . $get_group_info . "</td><td>" . $ownerName .
                                            "</td><td>" . $owner_status . "</td><td>" .

                                            "<a href='group.php?operator=manage&group_id=$get_group_id' > 
                                  <button type='button' class='btn btn-primary btn-sm' name='manage' >manage </button>
                                </a> " . "</td><td>" .
                                            "<a href='group.php?operator=delete&group_id=$get_group_id' > 
                                  <button type='button' class='btn btn-danger btn-sm' name='delete'>delete </button>
                                </a> " . "</td></tr>";
                                    } else {
                                        $owner_status = 'member';
                                        //echo table
                                        echo "<tr><td>" . $get_group_id . "</td><td>" . $get_group_info . "</td><td>" . $get_owner_id .
                                            "</td><td>" . $owner_status . "</td><td>" .

                                            "<a href='' > 
                                  <button type='button' class='btn btn-default btn-sm' name='unmodify' disabled='disabled' >unmodify </button>
                                    </a> "
                                            . "</td><td>" .
                                            "<a href='group.php?operator=exit_group&group_id=$get_group_id' > 
                                  <button type='button' class='btn btn-info btn-sm' name='exit'>Quit</button>
                                </a> " . "</td></tr>";
                                    }

                                }//while

                            } else {
                                echo "Error: find group_info function " . $sql_group_info . "<br>" . mysqli_error($conn);
                            }

                        }//while

                        ?>
                    </table>

                    <form action="" method="post">
                        <div class="form-group">
                            <label for="apply_group">Send request for joining a group</label>
                            <input type="text" class="form-control" id="apply_group" name="apply_group"
                                   placeholder="please enter group name "/>
                        </div>


                        <button type="submit" class="btn btn-primary btn-sm" name="submit" value="apply_group">request
                        </button>

                    </form>
                    <br/>

                    <!--  create new group button-->
                    <div class="button-group">
                        <a href='group.php?operator=create_new_group'>
                            <button type='button' class='btn btn-info btn-sm' name='create_new_group'>Create a new group
                            </button>
                        </a>
                    </div>

                    <?php

                    if ($create_group_status == 1) {
                        echo '<form action=""  method="post">
                            <div class="form-group">
                                <label for="group_info">Group Name</i>
                                <input type="text" class="form-control" id="group_info" name="group_info" placeholder="please enter a group name"/>
                            </div>
                            
                            
                            <button type="submit" class="btn btn-primary btn-sm" name="submit" value="create">create</button>
                            
                           
                            </form>';

                    }

                    ?>


                    <br>


                    <!-- check create status-->


                </div>
            </div>
        </div>
    </div>
<?php
include '../partials/footer.php';
?>