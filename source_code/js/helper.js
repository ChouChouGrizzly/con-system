/*
Developers:

Jinchen Hu 40080398

*/
function checkUser() {
    let user = document.getElementById("username").value;
    let invalid = document.getElementById("invalid-name");
    invalid.innerHTML = ""
    let reg = /^[a-zA-Z][a-zA-Z0-9]{3,15}$/;
    if (reg.test(user) === false) {
        invalid.innerHTML = "invalid username";
        return false;
    }
    return true;
}


function checkPwd() {
    let password = document.getElementById("password").value;
    let invalid = document.getElementById("invalid-psw");
    invalid.innerHTML = "";
    let reg = /^[a-zA-Z][a-zA-Z0-9]{3,15}$/;
    if (reg.test(password) === false) {
        invalid.innerHTML = "invalid password";
        return false;
    }
    return true;
}

function checkNewName(){
    let name = document.getElementById("name").value;
    let nameError = document.getElementById("user-error");
    nameError.innerHTML = "";
    let reg = /^[a-zA-Z][a-zA-Z0-9]{3,15}$/;
    if(reg.test(name) === false || name.toLowerCase() === 'admin'){
        nameError.innerHTML = "Username must be at least 3 letters and cannot be 'Admin'";
        return false;
    }
    return true;
}

function checkNewPassword(){
    let password = document.getElementById("password").value;
    let invalid = document.getElementById("psw-error");
    invalid.innerHTML = "";
    let reg = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/;
    if (reg.test(password) === false) {
        invalid.innerHTML = "Password should be at least 8 characters with 1 uppercase, 1 lowercase, and any special character";
        return false;
    }
    return true;
}

function checkEmail(){
    let email = document.getElementById('email').value;
    let invalid = document.getElementById('email-error');
    invalid.innerHTML = "";
    let reg = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
    if(reg.test(email) === false){
        invalid.innerHTML = "Invalid Email";
        return false;
    }
    return true;
}

function confirmPsw(){
    let password = document.getElementById("password").value;
    let confirm = document.getElementById("password_again").value;
    let invalid = document.getElementById('psw-confirm-error');
    invalid.innerHTML = "";
    if(password !== confirm){
        invalid.innerHTML = "Des not match password";
        return false;
    }
    return true;
}

function checkPercent(){
    let percent = document.getElementById('pct-share').value;
    let invalid = document.getElementById('pct-confirm-error');
    invalid.innerHTML = "";
    if(percent >= 1){
        invalid.innerHTML = "Place input a number less than 1";
        return false;
    }
    return true;
}

function checkGroup(option) {
    if (option.value === "2") {
        document.getElementById("group-list").style.display = "block";
    } else {
        document.getElementById("group-list").style.display = "none";
    }
}

function checkMeeting(option) {
    if (option.value === "meeting") {
        document.getElementById("meeting-time").style.display = "block";
    } else {
        document.getElementById("meeting-time").style.display = "none";
    }
}