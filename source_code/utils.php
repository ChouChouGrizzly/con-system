<?php
/*
Developers:

Jinchen Hu 40080398

*/
function displayWarning($msg)
{
    echo
        '<div class="alert alert-warning alert-dismissible show" role="alert">' . $msg . '
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>';
}

function displayError($msg)
{
    echo
        '<div class="alert alert-danger alert-dismissible show" role="alert">' . $msg . '
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>';
}

function displaySuccess($msg)
{
    echo
        '<div class="alert alert-success alert-dismissible show" role="alert">' . $msg . '
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>';
}

?>

<script>
    $(document).ready(function () {
        // show the alert
        setTimeout(function () {
            $(".alert").alert('close');
        }, 5000);
    });
</script>
