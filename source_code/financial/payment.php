<?php
/*
Financial records  page
*/
/*
Developers:
Sasa Zhang 25117151
Ruoshi Wu 27448724
Jinchen Hu 40080398

*/
include '../partials/check_login.php';
include '../partials/header.php';
include '../sql_config/connect_db.php';

$userid = $_SESSION['userid'];

//status assoc
$assoc_status = 0;
//system admin
if ($userid == 100) {
//find your associationID from con_association
    $sql_association = "select distinct(associationID) from condo_assoc ";
//execute query
    $result_association = $conn->query($sql_association);
//num of result
    $num = $result_association->num_rows;
//check if user is exist
    if ($num) {
        //$row = mysqli_fetch_array($result);
        $assoc_status = 1;

    } else {
        //echo "Error: find associationID function " . $sql_association . "<br>" . mysqli_error($conn);
    }
}//if userid==100
//execpt system admin
if ($_SESSION['isAdmin'] == 2 && $userid != 100) {
    //find your associationID from con_association
    $sql_association = "select associationID from condo_assoc where assoc_adminID='$userid' ";
//execute query
    $result_association = $conn->query($sql_association);
//num of result
    $num = $result_association->num_rows;
//check if user is exist
    if ($num) {
        //$row = mysqli_fetch_array($result);
        $assoc_status = 1;
    } else {
        //echo "Error: find associationID function 2 " . $sql_association . "<br>" . mysqli_error($conn);
    }
}

//if user
if ($_SESSION['isAdmin'] == 0) {
    //sql
    $sql_user_assoc = "select associationID from assoc_member_list where userID='$userid'";
    $result_association = $conn->query($sql_user_assoc);
    //num of result
    $num = $result_association->num_rows;
//check if user is exist
    if ($num) {
        //$row = mysqli_fetch_array($result);
        $assoc_status = 1;
    } else {
        //echo "Error: find associationID function 2 " . $sql_association . "<br>" . mysqli_error($conn);
    }
}


//refresh
if (isset($_GET['operator']) && $_GET['operator'] == 'refresh') {
    header("location:payment.php");
    exit;
}

//payment
$status_pay = 0;
if (isset($_GET['operator']) && $_GET['operator'] == 'payment') {
    $get_assoc_id = $_GET['assoc'];
    $_SESSION['get_assoc_id'] = $get_assoc_id;
    $status_pay = 1;

}

// form pay
$status_payed = 0;
if (isset($_POST['submit']) && $_POST['submit'] == 'pay') {
    if ($_POST['pay_num'] == '') {
        echo "Please enter amout";
    } else {
        $get_amount = $_POST['pay_num'];
        //$get_amount = (double) $get_amount;
        $assoc_id = $_SESSION['get_assoc_id'];
        //find the latest fee_id, then add 1 for new_fee_id
        $sql_m = "select max(feeID) from financial_record";
        $result3 = $conn->query($sql_m);
        //check if the email_recorded is empty
        $num_empty = $result3->num_rows;
        if ($num_empty) {
            $row = mysqli_fetch_array($result3);
            $fee_id = $row[0] + 1;
        } else {
            //initial fee_id
            $fee_id = 10000;
        }
        //get ratio option
        if (isset($_POST['optionradio']) && $_POST['optionradio'] == 'option1') {
            $option_come = 'Income';
        }
        if (isset($_POST['optionradio']) && $_POST['optionradio'] == 'option2') {
            $option_come = 'Outcome';
        }


        //get current time
        $time = date('Y-m-d');
        //insert payment
        $sql_pay = "insert into financial_record values('$fee_id', '$assoc_id', '$get_amount', '$userid', '$option_come' , '$time' )";
        if (mysqli_query($conn, $sql_pay)) {
            $status_payed = 1;
        } else {
//            echo '<div class="alert alert-danger" role="alert">Failed to pay!!!</div>';
            displayError("Failed to pay!");
        }

    }

}//pay

//payment history
$status_history = 0;
if (isset($_POST['submit']) && $_POST['submit'] == 'payment_history') {
    $his_assoc = $_SESSION['get_assoc_id'];
    if ($_SESSION['isAdmin'] == 1) {
        //find all records of association
        $sql_his_assoc = "select * from financial_record where associationID='$his_assoc'";
        $result_his = $conn->query($sql_his_assoc);
        $num_his = $result_his->num_rows;
        if ($num_his) {
            $status_history = 1;

        } else {
            echo '<div class="alert alert-danger" role="alert">Oops, no such history!!!</div>';
        }
    }//admin
    else {
        //find himself records of association
        $sql_his_assoc = "select feeID, associationID, fee, contractor_payer, type, date_of_payment from financial_record where associationID='$his_assoc' and contractor_payer='$userid' ";
        $result_his = $conn->query($sql_his_assoc);
        $num_his = $result_his->num_rows;
        if ($num_his) {
            $status_history = 1;
        } else {
            echo '<div class="alert alert-danger" role="alert">Oops, no such history!!!</div>';
        }
    }//user


}

//outcome , income
$status_income = 0;
$status_outcome = 0;
if (isset($_POST['submit']) && $_POST['submit'] == 'income_history') {
    $his_assoc = $_SESSION['get_assoc_id'];
    if ($_SESSION['isAdmin'] == 1) {
        //find all records of association
        $sql_assoc_income = "select feeID, associationID, fee, contractor_payer, type, date_of_payment from financial_record where associationID='$his_assoc' and type='Income'";

        $result_income = $conn->query($sql_assoc_income);
        $num_in = $result_income->num_rows;
        if ($num_in) {
            $status_income = 1;
        } else {
            echo '<div class="alert alert-danger" role="alert">Oops, no such history!!!</div>';
        }
    }//admin
    else {
        //find himself records of association
        $sql_assoc_income = "select feeID, associationID, fee, contractor_payer, type, date_of_payment from financial_record where associationID='$his_assoc' and contractor_payer='$userid' and type='Income' ";
        $result_income = $conn->query($sql_assoc_income);
        $num_in = $result_income->num_rows;
        if ($num_in) {
            $status_income = 1;
        } else {
            echo '<div class="alert alert-danger" role="alert">Oops, no such history!!!</div>';
        }
    }//user

}
if (isset($_POST['submit']) && $_POST['submit'] == 'outcome_history') {

    $his_assoc = $_SESSION['get_assoc_id'];
    if ($_SESSION['isAdmin'] == 1) {
        //find all records of association
        $sql_assoc_outcome = "select feeID, associationID, fee, contractor_payer, type, date_of_payment from financial_record where associationID='$his_assoc' and type='Outcome'";

        $result_outcome = $conn->query($sql_assoc_outcome);
        $num_out = $result_outcome->num_rows;
        if ($num_out) {
            $status_outcome = 1;

        } else {
            echo '<div class="alert alert-danger" role="alert">Oops, no such history!!!</div>';
        }
    }//admin
    else {
        //find himself records of association
        $sql_assoc_outcome = "select feeID, associationID, fee, contractor_payer, type, date_of_payment from financial_record where associationID='$his_assoc' and contractor_payer='$userid' and type='Income' and type='Outcome' ";
        $result_outcome = $conn->query($sql_assoc_outcome);
        $num_out = $result_outcome->num_rows;
        if ($num_out) {
            $status_outcome = 1;
        } else {
            echo '<div class="alert alert-danger" role="alert">Oops, no such history!!!</div>';
        }
    }//user

}

?>


    <div class="d-flex" id="wrapper">

    <div class="bg-light border-right sidebar" id="sidebar-wrapper">
        <div class="sidebar-heading"><h4>&nbsp; &nbsp;&nbsp; &nbsp;Financial</h4></div>
        <div class="list-group list-group-flush">
            <a href="financial_state.php" class="list-group-item list-group-item-action bg-light">Financial State</a>
            <a href="payment.php" class="list-group-item list-group-item-action bg-light">Payment & History
                </a>
        </div>
    </div>

    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-0">Payment & History</h4>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-hover table-bordered" style="width: auto">
                        <tr>
                            <th style="align-content: center">Association ID</th>
                            <th style="align-content: center">Payment</th>
                        </tr>
                        <?php
                        if ($assoc_status == 1) {
                            //find your all groups from group_leader
                            while ($row = mysqli_fetch_array($result_association)) {
                                //find each group info
                                $get_assoc_id = $row['associationID'];
                                //echo
                                echo "<tr><td>" . $get_assoc_id .
                                    "</td><td>" .

                                    "<a href='payment.php?operator=payment&assoc=$get_assoc_id' > 
                                  <button type='button' class='btn btn-primary btn-sm' name='payment' > payment </button>
                                </a> " . "</td><tr>";

                            }//while
                        } else {
                            echo '<div class="alert alert-danger" role="alert">You are not registered in any association</div>';
                        }
                        ?>
                    </table>
                    <!--  create new group button-->
                    <br>

                    <br>

                    <!-- check create status-->
                    <?php
                    if ($status_pay == 1) {
                        echo "<h4 style='align-content: center'>Association - " . $_SESSION['get_assoc_id'] . "</h4>";
                        echo '<form action=""  method="post">
                        <div class="form-group">
                        <label for="pay_num" class="col-md-3 col-form-label form-control-label">Pay</label>
                        <div class="col-md-9">
                       <input type="number" class="form-control" id="pay_num" name="pay_num" maxlength="5" size="4" width="200px" placeholder="please enter amount "/> 
                       </div> <br>';
                        echo ' <div class="form-group"><div class="col-md-9">
                        <button type="submit" class="btn btn-primary btn-sm" name="submit" value="pay">pay</button></div>
                        </div>';

                        echo '<hr>';

                        if ($_SESSION['isAdmin'] == 1) {
                            echo '<div class="form-group"><div class="form-check form-check-inline">&nbsp;&nbsp;&nbsp;
                        <lable class="form-check-label">
                            <input class="form-check-input" type="radio" name="optionradio" id="radio1" value="option1" checked> Revenue
                        </lable></div>
                        <div class="form-check form-check-inline">
                        <lable class="form-check-label">
                            <input class="form-check-input" type="radio" name="optionradio" id="radio2" value="option2"> Outgoings
                        </lable>
                        </div></div>';
                        }




                        echo '&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-info btn-sm" name="submit" value="payment_history">payment history</button> &nbsp;';

                        echo '<button type="submit" class="btn btn-success btn-sm" name="submit" value="income_history">Income history</button>&nbsp;';

                        echo '<button type="submit" class="btn btn-primary btn-sm" name="submit" value="outcome_history">Outcome history</button>';


                        echo '</div>                     
                  </form>';
                    }//if


                    //payed
                    if ($status_payed == 1) {
                        echo '<div class="alert alert-primary" role="alert">Payment Received !!!</div>';
                    }
                    //payment history
                    if ($status_history == 1) {
                        echo '
                <table class="table table-striped table-hover table-bordered">
                <h4>Payment History</h4>
                <tr>
                    <th style="align-content: center">Fee ID</th>
                    <th style="align-content: center">Amount</th>
                    <th style="align-content: center">UserID</th>
                    <th style="align-content: center">Rational</th>
                    <th style="align-content: center">Date of payment</th>      
                </tr>
                
                ';

                        while ($row_his = mysqli_fetch_array($result_his)) {
                            echo "<tr><td>" . $row_his['feeID'] . "</td><td>" . $row_his['fee'] . "</td><td>" . $row_his['contractor_payer'] .
                                "</td><td>" . $row_his['rationales'] . "</td><td>" . $row_his['date_of_payment'] . "</td><tr>";
                        }

                        echo ' </table>';

                    }

                    //admin history
                    if ($status_income == 1) {

                        echo '<table class="table table-striped table-hover table-bordered" style="width: auto;">
                <h4>Income History</h4>
                <tr>
                    <th style="align-content: center">Fee ID</th>
                    <th style="align-content: center">Amount</th>
                    <th style="align-content: center">UserID</th>
                    <th style="align-content: center">Rational</th>
                    <th style="align-content: center">Date of payment</th>      
                </tr>';
                        $total_income = 0;
                        while ($row_income = mysqli_fetch_array($result_income)) {
                            $total_income = $total_income + $row_income['fee'];
                            echo "<tr><td>" . $row_income['feeID'] . "</td><td>" . $row_income['fee'] . "</td><td>" . $row_income['contractor_payer'] .
                                "</td><td>" . $row_income['rationales'] . "</td><td>" . $row_income['date_of_payment'] . "</td><tr>";

                        }
                        echo "<tr>Total Income : " . $total_income . "</tr>";


                        echo ' </table>';


                    }

                    if ($status_outcome == 1) {

                        echo '<table class="table table-striped table-hover table-bordered" style="width: auto;">
                <h4>Outcome History</h4>
                <tr>
                    <th style="align-content: center">Fee ID</th>
                    <th style="align-content: center">Amount</th>
                    <th style="align-content: center">UserID</th>
                    <th style="align-content: center">Rational</th>
                    <th style="align-content: center">Date of payment</th>      
                </tr>';
                        $total_outcome = 0;
                        while ($row_outcome = mysqli_fetch_array($result_outcome)) {
                            $total_outcome = $total_outcome + $row_outcome['fee'];
                            echo "<tr><td>" . $row_outcome['feeID'] . "</td><td>" . $row_outcome['fee'] . "</td><td>" . $row_outcome['contractor_payer'] .
                                "</td><td>" . $row_outcome['rationales'] . "</td><td>" . $row_outcome['date_of_payment'] . "</td><tr>";

                        }
                        echo "<tr>Total Outcome : " . $total_outcome . "</tr>";


                        echo ' </table>';


                    }

                    ?>

                </div>
            </div>
        </div></div>
    </div>
<?php
include '../partials/footer.php';
?>