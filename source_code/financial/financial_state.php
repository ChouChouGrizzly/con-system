<?php
/*
Financial records  page
*/
/*
Developers:
Sasa Zhang 25117151
Ruoshi Wu 27448724
Jinchen Hu 40080398

*/
include '../partials/check_login.php';
include '../partials/header.php';
include '../sql_config/connect_db.php';

$userid = $_SESSION['userid'];

//status assoc
$per_condo_status = 0;
//system admin
if ($userid == 100) {
//find your associationID from con_association
    $sql_association = "select condoID from condo_unit  ";
//execute query
    $result_association = $conn->query($sql_association);
//num of result
    $num = $result_association->num_rows;
//check if user is exist
    if ($num) {
        //$row = mysqli_fetch_array($result);
        $per_condo_status = 1;

    } else {
        //echo "Error: find associationID function " . $sql_association . "<br>" . mysqli_error($conn);
    }
}//if userid==100
//execpt system admin
if ($_SESSION['isAdmin'] == 2 && $userid != 100) {
    //find your associationID from con_association
    $sql_association = "select condoID from condo_unit where buildingID=(select associationID from assoc_member_list where userID='$userid')";
//execute query
    $result_association = $conn->query($sql_association);
//num of result
    $num = $result_association->num_rows;
//check if user is exist
    if ($num) {
        //$row = mysqli_fetch_array($result);
        $per_condo_status = 1;
    } else {
        //echo "Error: find associationID function 2 " . $sql_association . "<br>" . mysqli_error($conn);
    }
}
if ($_SESSION['isAdmin'] == 0) {
    //find your associationID from con_association
    $sql_association = "select condoID from condo_unit where ownerID='$userid' ";
//execute query
    $result_association = $conn->query($sql_association);
//num of result
    $num = $result_association->num_rows;
//check if user is exist
    if ($num) {
        //$row = mysqli_fetch_array($result);
        $per_condo_status = 1;
    } else {
        //echo "Error: find associationID function 2 " . $sql_association . "<br>" . mysqli_error($conn);
    }

}


//refresh
if (isset($_GET['operator']) && $_GET['operator'] == 'refresh') {
    header("location:financial_state.php");
    exit;
}

//condo details
$condo_detail_status = 0;
if (isset($_GET['operator']) && $_GET['operator'] == 'search_money') {
    $old_condo_id = $_GET['condo_id2'];
    $_SESSION['condo_id2'] = $old_condo_id;
    //sql
    $sql_per_fee = "select cost_sm_condo, cost_sm_parking, cost_sm_storage from condo_assoc
where associationID=(select b.associationID from condo_unit c, building b where c.buildingID=b.buildingID and c.condoID='$old_condo_id') ";
    $result_per_cost = $conn->query($sql_per_fee);
    $num_cost = $result_per_cost->num_rows;
    if ($num_cost) {
        $condo_detail_status = 1;
    } else {
        echo "Fail: No found Cost!";
    }

}


?>
<div class="d-flex" id="wrapper">
<div class="bg-light border-right sidebar" id="sidebar-wrapper">
    <div class="sidebar-heading"><h4>&nbsp; &nbsp;&nbsp; &nbsp;Financial</h4></div>
    <div class="list-group list-group-flush">
        <a href="financial_state.php" class="list-group-item list-group-item-action bg-light">Financial State</a>
        <a href="payment.php" class="list-group-item list-group-item-action bg-light">Payment & History
        </a>
    </div>
</div>

<div id="page-content-wrapper">
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h4 class="mb-0">Financial States</h4>
            </div>
            <div class="card-body">

                    <!-- check create status-->
                    <?php
                    if ($condo_detail_status == 1) {
                        while ($row_d = mysqli_fetch_array($result_per_cost)) {
                            $sm_condo = $row_d['cost_sm_condo'];
                            $sm_park = $row_d['cost_sm_parking'];
                            $sm_storage = $row_d['cost_sm_storage'];
                            $area_status = 1;

                        }

                        $id_condo = $_SESSION['condo_id2'];
                        //sql
                        $sql_c_area = "select size  from condo_unit where condoID='$id_condo'  ";
                        $result_c_condo = $conn->query($sql_c_area);
                        $num_c = $result_c_condo->num_rows;
                        if ($num_c) {
                            //sql
                            $sql_c_park = "select sum(size) as p_size from  parking where condoID='$id_condo' ";
                            $result_c_park = $conn->query($sql_c_park);
                            $num_c_pa = $result_c_park->num_rows;
                            if ($num_c_pa) {
                                //sql
                                $sql_c_st = "select sum(size) as st_size from storage where condoID='$id_condo'";
                                $result_c_st = $conn->query($sql_c_st);
                                $num_c_st = $result_c_st->num_rows;
                                if ($num_c_st) {
                                    $row_c = mysqli_fetch_array($result_c_condo);
                                    $row_c_pa = mysqli_fetch_array($result_c_park);
                                    $row_c_st = mysqli_fetch_array($result_c_st);
                                    $c_condo = $row_c[0];
                                    $c_park = $row_c_pa[0];
                                    $c_st = $row_c_st[0];

                                    //calculate total cost
                                    if ($area_status == 1) {
                                        $total_condo = $c_condo * $sm_condo;
                                        $total_park = $c_park * $sm_park;
                                        $total_st = $c_st * $sm_storage;
                                        $total = $total_condo+$total_park+$total_st;
                                        echo '<div class="alert alert-success" role="alert">
                                               <h4 class="alert-heading">CONDO '.$id_condo.' TOTAL FEE: '. $total .'$</h4>
                                              
                                                </div>';

                                    } else {
                                        echo "No enough num to calculate!";
                                    }


                                } else {
                                    echo "No found storage size!";
                                }

                            } else {
                                echo "No found Parking Size!";
                            }


                        } else {
                            echo "No Found condo size!";
                        }


                        echo ' </table>';


                    }//if


                    ?>

                    <table class="table table-striped table-hover" style="width: 600px;">
                        <thead>
                        <tr>
                            <th style="align-content: center" scope="col">Condo ID</th>
                            <th style="align-content: center" scope="col">Finance details</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if ($per_condo_status == 1) {
                            //find your all groups from group_leader
                            while ($row = mysqli_fetch_array($result_association)) {
                                //find each group info
                                $get_condo_id = $row['condoID'];

                                //echo
                                echo "<tr><td>" . $get_condo_id .
                                    "</td><td>" .

                                    "<a href='financial_state.php?operator=search_money&condo_id2=$get_condo_id'> 
                                  <button type='button' class='btn btn-primary btn-sm' name='details'>details</button>
                                </a> " . "</td><tr>";

                            }//while
                        } else {
                            echo "No Condo!";
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php
include '../partials/footer.php';
?>
