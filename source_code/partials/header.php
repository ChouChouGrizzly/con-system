<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';
require_once $_SERVER['DOCUMENT_ROOT']. '/utils.php';
?>
<!--/*-->
<!--Developers:-->
<!--Sasa Zhang 25117151-->
<!--Ruoshi Wu 27448724-->
<!--Jinchen Hu 40080398-->
<!---->
<!--*/-->
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Font Awesome -->
    <link href="https://use.fontawesome.com/releases/v5.11.2/css/all.css" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <!-- Local CSS Config -->
    <link rel="stylesheet" type="text/css" href=<?php echo BASE_URL . "css/main.css" ?>>
    <script type="text/javascript">
        $(document).ready(function () {
            let url = window.location;
            $('#head-navbar a[href="' + url + '"]').parent().addClass('active');
            $('ul.nav-link').filter(function () {
                return this.href === url;
            }).parent().addClass('active');
        });
    </script>

    <title>Condo Association System</title>

</head>
<body>
<script src="../js/helper.js"></script>

<div class="container-fluid top">
    <a class="cms-logo">Condo Network System</a>
    <div class="montreal-img"><img src="<?php echo BASE_URL . "images/Montreal_Panorama.jpg" ?>" alt="montreal"></div>
</div>
<header class="sticky-top" id="header">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark flex-column flex-md-row menu-navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#"><i class="fas fa-city"></i></a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse w-100 order-1 order-md-0 dual-collapse2">
                <ul class="navbar-nav mr-auto pt-1" id="head-navbar">
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo BASE_URL . "index.php" ?>">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <?php
                    if(isset($_SESSION['isAdmin'])){
                        if($_SESSION['isAdmin']){
                            echo'<li class="nav-item">
                                    <a class="nav-link" href=" '.BASE_URL . 'group/admin_group.php">Group</span></a>
                                 </li>';
                        }else{
                            echo'<li class="nav-item">
                                    <a class="nav-link" href=" '.BASE_URL . 'group/group.php">Group</span></a>
                                 </li>';
                        }
                    }
                       ?>
                    <?php
                    if (isset($_SESSION['isAdmin'])) {
                        echo '<li class="nav-item dropdown group-dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="group-navbarDropdownMenuLink" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Posting
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="' . BASE_URL . 'posting/post.php">New posting</a>
                            <a class="dropdown-item" href="' . BASE_URL . 'posting/post_list.php">General</a>
                            <a class="dropdown-item" href="' . BASE_URL . 'posting/ads.php">Ads</a>
                            <a class="dropdown-item" href="' . BASE_URL . 'posting/meeting.php">Meeting</a>
                            <a class="dropdown-item" href="' . BASE_URL . 'posting/vote.php">Vote</a>
                        </div>
                    </li>
                    
                     <li class="nav-item dropdown email-dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="email-navbarDropdownMenuLink" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Financial
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href=" ' . BASE_URL . 'financial/financial_state.php" >Financial State</a>
                            <a class="dropdown-item" href="' . BASE_URL . 'financial/payment.php" >Payment</a>
                        </div>
                    </li>

                    <li class="nav-item dropdown email-dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="email-navbarDropdownMenuLink" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Email
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href=" ' . BASE_URL . 'email/compose.php" >Compose</a>
                            <a class="dropdown-item" href="' . BASE_URL . 'email/inbox.php" >Inbox</a>
                            <a class="dropdown-item" href="' . BASE_URL . 'email/sent.php">Sent</a>
                        </div>
                    </li>';
                    }
                    ?>

                    <?php
                    if (isset($_SESSION['isAdmin']) && $_SESSION['isAdmin']) {
                        echo '
                        <li class="nav-item dropdown usr-mng-dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Admin Management
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="' . BASE_URL . 'admin_management/create_user.php">Create Members</a>
                                <a class="dropdown-item" href="' . BASE_URL . 'admin_management/delete_user.php">Delete Members</a>';
                              if($_SESSION['userid']==100) {
                               echo' <a class="dropdown-item" href="' . BASE_URL . 'admin_management/assign_assoc_admin.php">Assign Admin</a>';}
                             
                           echo"</div>";
                       echo"</li>";

                    }
                    ?>
                </ul>
            </div>


            <?php
            if (isset($_SESSION['isAdmin'])) {
                echo '<div class="navbar-collapse collapse order-2 dual-collapse2 left-menu w-100">
                <ul class="navbar-nav ml-auto w-auto">
                    <li class="nav-item">
                        <a class="nav-link"
                           href="' . BASE_URL . 'profile/profile.php">
                            <i class="far fa-user"></i>  ' . $_SESSION['username'] . '</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link"
                           href="' . BASE_URL . 'partials/logout.php">Sign Out</a>
                    </li>
                </ul>
            </div>';
            } else {

                echo '<div class="navbar-collapse collapse order-2 dual-collapse2 left-menu w-100">
                <ul class="navbar-nav ml-auto w-auto">
                    <li class="nav-item">
                        <a class="nav-link" aria-readonly="true"
                           href="#">
                            <i class="far fa-user"></i> Guest</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link"
                           href="' . BASE_URL . 'partials/login.php">Sign In</a>
                    </li>
                </ul>
            </div>';
            }
            ?>
        </div>
    </nav>
</header>

