<?php
// Launch the session
session_start();
// Connect to the database
include "../sql_config/connect_db.php";
/*
Developers:
Sasa Zhang 25117151
Ruoshi Wu 27448724
Jinchen Hu 40080398

*/
$user = $password = "";
$userErr = $pwdErr = "";
$notExistErr = "";

if (isset($_POST["username"]) && isset($_POST["password"])) {
    // convert html string to sql string
    $user = $_POST['username'];
    $password = $_POST['password'];
    // query to the database
    $sql = "SELECT * FROM user WHERE userName='$user' AND pwd='$password'";
    $result = $conn->query($sql) or die($conn->error);
    // check the number of query results
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        // store in the session
        $_SESSION['username'] = $row['userName'];
        $_SESSION['userid'] = $row['userId'];
        $_SESSION['isAdmin'] = $row['isAdmin'];
        $_SESSION['email'] = $row['Email'];
        $_SESSION['DOB'] = $row['DOB'];
        $_SESSION['address'] = $row['address'];
        $_SESSION['password'] = $row['pwd'];
        $_SESSION['loggedin'] = 1;

        $ass = $conn->query("select associationID from assoc_member_list where userID = ". $_SESSION['userid']) or die($conn->error);
        $ass_row = $ass->fetch_assoc();
        $_SESSION['assid'] = $ass_row['associationID'];
        // if the username is Admin, redirect to profile management
        if ($_SESSION['isAdmin']) {
            header("location:" . BASE_URL . "profile/profile_edit.php");
            die();
        } else {
            header("location:" . BASE_URL . "index.php");
            die();
        }
    } else {
        $notExistErr = "Account does not exist";
    }
}

$conn->close();
?>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login Page</title>
    <!--Bootsrap 4 CDN-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!--Fontawesome CDN-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
          integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!--Custom styles-->
    <link rel="stylesheet" type="text/css" href="../css/login.css">
    <script src="../js/helper.js"></script>
</head>
<body>
<div class="container">
    <form method="post" action="">
        <div class="d-flex justify-content-center h-100">
            <div class="card">
                <div class="card-header">
                    <h3>Sign In</h3>
                </div>
                <div class="card-body">
                    <form>
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><label for="username"><i class="fas fa-user"></i></label></span>
                            </div>
                            <input type="text" id="username" name="username" class="form-control" placeholder="username"
                                   onblur="checkUser()"/>

                        </div>
                        <div id="invalid-name" class="invalid-input"><?php echo $userErr; ?></div>


                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><label for="password"><i class="fas fa-key"></i></label></span>
                            </div>
                            <input type="password" id="password" name="password" class="form-control"
                                   placeholder="password" onblur="checkPwd()"/>
                        </div>

                        <div id="invalid-psw" class="invalid-input"><?php echo $pwdErr; ?></div>

                        <div class="row align-items-center remember">
                            <label>
                                <input type="checkbox">
                            </label>Remember Me
                        </div>

                        <div id="invalid-user" class="invalid-input"><?php echo $notExistErr; ?></div>

                        <div class="form-group">
                            <input type="submit" value="Login" class="btn float-right login_btn"/>
                        </div>
                    </form>
                </div>
                <div class="card-footer">
                    <div class="d-flex justify-content-center links">
                        Don't have an account?<a href="#">Contact Admin</a>
                    </div>
                    <div class="d-flex justify-content-center">
                        <a href="#">Forgot your password?</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<ul class="slideshow">
    <li></li>
    <li></li>
    <li></li>
    <li></li>
</ul>
</body>
</html>
