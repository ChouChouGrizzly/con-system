<?php
session_start();
require_once $_SERVER['DOCUMENT_ROOT'].'/config.php';
if (isset($_SESSION["username"])) {
    session_unset();
    session_destroy();
    header("location:".BASE_URL."index.php");
}
/*
Developers:
Sasa Zhang 25117151
Ruoshi Wu 27448724
Jinchen Hu 40080398

*/
