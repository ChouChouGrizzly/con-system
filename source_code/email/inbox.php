<?php
include '../partials/check_login.php';
include '../partials/header.php';
include '../sql_config/connect_db.php';
/*
Developers:
Sasa Zhang 25117151
Ruoshi Wu 27448724
Jinchen Hu 40080398

*/

$receiver_id = $_SESSION['userid'];

if(isset($_GET['action'])&&isset($_GET['record_id'])) {
    $action = $_GET['action'];
    $action_id = $_GET['record_id'];
    //mark as read
    if ($action == 0) {
        $conn->query("UPDATE email SET isRead = 0 where emailId = '$action_id'");
    } else {
        $conn->query("UPDATE email SET isRead = 1 where emailId = '$action_id'");
    }

}
?>


<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light border-right sidebar" id="sidebar-wrapper">
        <div class="sidebar-heading"><h4>&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;Email</h4></div>
        <div class="list-group list-group-flush">
            <a href="compose.php" class="list-group-item list-group-item-action bg-light">Compose</a>
            <a href="inbox.php" class="list-group-item list-group-item-action bg-light">Inbox</a>
            <a href="sent.php" class="list-group-item list-group-item-action bg-light">Sent</a>
        </div>
    </div>

    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-0">Inbox</h4>
                </div>
                <div class="card-body">
                    <?php

                    $result = $conn->query("SELECT * FROM email_record where receiver_id = '$receiver_id'") or die($conn->error);
                    if ($result->num_rows > 0) {
                        echo '<table style="width: auto" class="table table-striped table-hover">';
                        echo '<thead><tr> 
                            <th scope="col">From</th>
                            <th scope="col">Email</th>
                            <th scope="col">Title</th>
                            <th scope="col">Content</th>
                            <th scope="col">Date</th>
                            <th scope="col">Mark AS</th>
                            </tr></thead><tbody>';
                        while ($row = $result->fetch_assoc()) {
                            $sender_id = $row['sender_id'];
                            $record_id = $row['recordID'];

                            $sender = $conn->query("SELECT userName, Email FROM user where userId = $sender_id ") or die($conn->error);
                            $senderRow = $sender->fetch_assoc();
                            $senderName = $senderRow['userName'];
                            $senderEmail = $senderRow['Email'];
                            $message = $conn->query("SELECT * FROM email where emailId = $record_id ") or die($conn->error);
                            $messageRow = $message->fetch_assoc();
                            $isRead = $messageRow['isRead'];

                            echo '<tr>';
                            echo '<td>' . $senderName . '</td>';
                            echo '<td>' . $senderEmail . '</td>';
                            echo '<td>' . $messageRow["title"] . '</td>';
                            echo '<td>' . $messageRow["content"] . '</td>';
                            echo '<td>' . $messageRow["createdDate"] . '</td>';

                            if ($isRead == 1) {
                                echo '<td><a href="inbox.php?action=0&record_id=' . $record_id . '"><button type="button" class="btn btn-sm btn-success">unseen</button></td>';
                            } else {
                                echo '<td><a href="inbox.php?action=1&record_id=' . $record_id . '"><button type="button" class="btn btn-sm btn-primary">read</button></td>';
                            }
                            echo '</tr>';
                        }
                        echo '</tbody></table>';
                    } else {
                        echo '<div class="alert alert-primary">Inbox is empty</div>';
                    }

                    ?>
                </div>
            </div>
        </div>
    </div>

</div>


<?php
include '../partials/footer.php';
?>
