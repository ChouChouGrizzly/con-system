<?php
include '../partials/check_login.php';
include '../partials/header.php';
include '../sql_config/connect_db.php';

/*
Developers:
Sasa Zhang 25117151
Ruoshi Wu 27448724
Jinchen Hu 40080398

*/

$emailErr = $titleErr = $contentErr = "";


if (isset($_POST['send'])) {
    $to = $_POST['to'];
    $title = htmlspecialchars($_POST['title']);
    $content = htmlspecialchars($_POST['content']);
    // check if the email exists
    $sql1 = "SELECT userId FROM user where Email = '$to'";
    $receiver = $conn->query($sql1) or die($conn->error);
    if ($receiver->num_rows > 0) {
        $receiverRow = $receiver->fetch_assoc();
        $receiverId = $receiverRow['userId'];
        $sendId = $_SESSION['userid'];
        $date = date('Y-m-d H:i:s', time());
        //update tables
        $conn->query("INSERT INTO email VALUES (NULL, '$title', '$content', now(), 0)") or die($conn->error);

        $messageId = $conn->insert_id;
        $conn->query("INSERT INTO email_record VALUES ('$sendId', '$receiverId', '$messageId')") or die($conn->error);
        $conn->close();

//        echo '<script> alert("Email Sent !!!") </script>';
         displaySuccess("Email Sent!");
    } else {
        $emailErr = 'The Email Address does not exist';
    }


}


//include '../partials/header.php';
?>
<script src="../js/helper.js"></script>

<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light border-right sidebar" id="sidebar-wrapper">
        <div class="sidebar-heading"><h4>&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;Email</h4></div>
        <div class="list-group list-group-flush">
            <a href="compose.php" class="list-group-item list-group-item-action bg-light">Compose</a>
            <a href="inbox.php" class="list-group-item list-group-item-action bg-light">Inbox</a>
            <a href="sent.php" class="list-group-item list-group-item-action bg-light">Sent</a>
        </div>
    </div>

    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-0">Compose an Email</h4>
                </div>
                <div class="card-body">
                    <form id=compose-email class="" method="post"
                          action="">

                        <div class="form-group row row-bottom-margin">
                            <label for="to" class="col-md-3 col-form-label form-control-label">To</label>
                            <div class="col-md-9">
                                <input name="to" id="to" class="form-control" type="email" placeholder="please input the email address"
                                       onblur="checkEmail()" required/>
                                <div class="invalid-input" id="email-error"><?php echo $emailErr; ?></div>
                            </div>
                        </div>


                        <div class="form-group row row-bottom-margin">
                            <label for="title" class="col-md-3 col-form-label form-control-label"> Title
                            </label>
                            <div class="col-md-9">
                                <input name="title" id="title" class="form-control" type="text"
                                       required/>
                                <div class="invalid-input" id="title-error"><?php echo $titleErr; ?></div>
                            </div>
                        </div>

                        <div class="form-group row row-bottom-margin">
                            <label for="content" class="col-md-3 col-form-label form-control-label"> Content
                            </label>
                            <div class="col-md-9">
                                <textarea id="content" class="form-control" name="content" rows="5" cols="50"
                                          required></textarea>
                                <div class="invalid-input" id="content-error"><?php echo $contentErr; ?></div>

                            </div>
                        </div>

                        <div class="form-group row row-bottom-margin">
                            <label class="col-md-3 col-form-label form-control-label"></label>
                            <div class="col-md-9">
                                <input type="reset" class="btn btn-secondary" value="Cancel">
                                <input type="submit" name="send" id="send" class="btn btn-primary" value="send">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>


<?php
include '../partials/footer.php';
?>
