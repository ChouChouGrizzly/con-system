<?php
include '../partials/check_login.php';
include '../partials/header.php';
include '../sql_config/connect_db.php';
/*
Developers:
Sasa Zhang 25117151
Ruoshi Wu 27448724
Jinchen Hu 40080398

*/
$titleErr = $scopeErr = $contentErr = $timeErr = $typeErr = $timeErr = $groupErr = "";
$userid = $_SESSION['userid'];
$assid = $_SESSION['assid'];

$posted = 0;
// general
if (isset($_POST['submit'])) {

    $topic = $_POST['title'];
    $scope = (int)$_POST['scope'];
    $content = $_POST['content'];
    $type = $_POST['type'];
    $select_group = $_POST['group-name'];
    $assid = $_SESSION['assid'];

    if ($scope == 4) {
        $scopeErr = "Please select the scope";
    } elseif ($scope == 2 && $select_group == "0") {
        $groupErr = "Please select one group";
    } elseif ($type == "4") {
        $typeErr = "Please select the posting type";
    } else {

        if ($scope == 2) {
            $scope = (int)$select_group;
        }

        if ($scope == 1) {
            $scope = $assid;
        }

        $conn->query("INSERT INTO post values (NULL, now(), now(), '$content', '$userid', NULL, '$scope','$type', '$topic' )") or die($conn->error);
        $post_id = $conn->insert_id;


        if ($type == 'ads') {

            $conn->query("INSERT INTO ads values ('$post_id','unsold')") or die($conn->error);
        }

        if ($type == 'contract') {
            $conn->query("INSERT INTO contract values ('$post_id', 'Done', '$content')") or die($conn->error);
        }

        if ($type == 'vote') {
            $conn->query("INSERT INTO vote values ('$post_id', 0, 0)") or die($conn->error);

            $email_title = "**VOTES**" . $topic;
            if ($scope == 0) {
                $all_user = $conn->query("select userId from user");
                while ($all_row = $all_user->fetch_assoc()) {
                    $vote_user = $all_row['userId'];
                    $conn->query("insert into vote_list values ('$vote_user', '$post_id', 0)");

                    $conn->query("insert into email values (null, '$email_title', '$content',now(), 0)");
                    $record_id = $conn->insert_id;
                    $conn->query("insert into email_record values ('$userid', '$vote_user', '$record_id' )");
                }
            } elseif ($scope <= 10) {
                $ass_user = $conn->query("select userID from assoc_member_list where associationID = '$scope'");
                while ($ass_row = $ass_user->fetch_assoc()) {
                    $ass_vote_user = $ass_row['userID'];
                    $conn->query("insert into vote_list values ('$ass_vote_user', '$post_id', 0)");

                    $conn->query("insert into email values (null, '$email_title', '$content',now(), 0)");
                    $record_id = $conn->insert_id;
                    $conn->query("insert into email_record values ('$userid', '$ass_vote_user', '$record_id' )");
                }
            } else {
                $group_user = $conn->query("select userID from group_member_list where groupsID = '$scope'");
                while ($group_row = $group_user->fetch_assoc()) {
                    $group_vote_user = $group_row['userID'];
                    $conn->query("insert into vote_list values ('$group_vote_user', '$post_id', 0)");

                    $conn->query("insert into email values (null, '$email_title', '$content',now(), 0)");
                    $record_id = $conn->insert_id;
                    $conn->query("insert into email_record values ('$userid', '$group_vote_user', '$record_id' )");
                }
            }
        }

        if ($type == 'meeting') {
            $start = $_POST['start'];
            $end = $_POST['end'];
            $conn->query("INSERT INTO meeting values ('$post_id', '$start', '$end' )") or die($conn->error);

            $email_title = "**MEETING**" . $topic . "from " . $start . " to " . $end;
            if ($scope == 0) {
                $all_user = $conn->query("select userId from user");
                while ($all_row = $all_user->fetch_assoc()) {
                    $vote_user = $all_row['userId'];

                    $conn->query("insert into email values (null, '$email_title', '$content',now(), 0)");
                    $record_id = $conn->insert_id;
                    $conn->query("insert into email_record values ('$userid', '$vote_user', '$record_id' )");
                }
            } elseif ($scope <= 10) {
                $ass_user = $conn->query("select userID from assoc_member_list where associationID = '$scope'");
                while ($ass_row = $ass_user->fetch_assoc()) {
                    $ass_vote_user = $ass_row['userID'];

                    $conn->query("insert into email values (null, '$email_title', '$content',now(), 0)");
                    $record_id = $conn->insert_id;
                    $conn->query("insert into email_record values ('$userid', '$ass_vote_user', '$record_id' )");
                }
            } else {
                $group_user = $conn->query("select userID from group_member_list where groupsID = '$scope' and userID <> '$userid'");
                while ($group_row = $group_user->fetch_assoc()) {
                    $group_vote_user = $group_row['userID'];

                    $conn->query("insert into email values (null, '$email_title', '$content',now(), 0)");
                    $record_id = $conn->insert_id;
                    $conn->query("insert into email_record values ('$userid', '$group_vote_user', '$record_id' )");
                }
            }

        }

        $posted = 1;
    }


}


// vote
if ($posted == 1) {

    displaySuccess('Successfully posted !!!');

}
?>


<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light border-right sidebar" id="sidebar-wrapper">
        <div class="sidebar-heading"><h4>&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;Posting</h4></div>
        <div class="list-group list-group-flush">
            <a href="post.php" class="list-group-item list-group-item-action bg-light">Post</a>
            <a href="post_list.php" class="list-group-item list-group-item-action bg-light">General</a>
            <a href="ads.php" class="list-group-item list-group-item-action bg-light">Ads</a>
            <a href="meeting.php" class="list-group-item list-group-item-action bg-light">Meeting</a>
            <a href="vote.php" class="list-group-item list-group-item-action bg-light">Vote</a>
            <a href="contracts.php" class="list-group-item list-group-item-action bg-light">Contract</a>
        </div>
    </div>

    <!-- Page Content -->


    <div id="page-content-wrapper">
        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-0">Compose a post</h4>
                </div>
                <div class="card-body">
                    <form id=write-post class="" method="post"
                          action="">
                        <div class="form-group row row-bottom-margin">
                            <label for="scope" class="col-md-3 col-form-label form-control-label"> Who can see?
                            </label>
                            <div class="col-md-9">
                                <select class="custom-select mr-sm-2" name="scope" id="scope"
                                        onchange="checkGroup(this)" required>
                                    <option value="4" selected>Choose</option>
                                    <option value="0">Public</option>
                                    <option value="1">Association</option>
                                    <option value="2">Group</option>
                                    <option value="-1">Private</option>
                                </select>
                                <div class="invalid-input" id="scope-error"><?php echo $scopeErr; ?></div>
                            </div>
                        </div>

                        <div id="group-list" style="display: none">
                            <div class="form-group row row-bottom-margin">
                                <label for="group-name" class="col-md-3 col-form-label form-control-label"> Group Name
                                </label>
                                <div class="col-md-9">
                                    <select class="custom-select mr-sm-2" name="group-name" id="group-name" required>

                                        <?php

                                        $groups = $conn->query("SELECT group_owner.groupName, group_owner.groupsID from group_member_list join group_owner on group_owner.groupsID = group_member_list.groupsID where userID = '$userid'");
                                        if (!is_null($groups) && $groups->num_rows > 0) {
                                            echo '<option value="0" selected>Choose</option>';
                                            while ($row = $groups->fetch_assoc()) {
                                                $group_name = $row['groupName'];
                                                $group_id = $row['groupsID'];
                                                echo "<option value='$group_id'>$group_name</option>";
                                            }
                                        } else {
                                            echo '<option value="0" selected>You are not in any group</option>';
                                        }
                                        ?>
                                    </select>
                                    <div class="invalid-input" id="group-error"><?php echo $groupErr; ?></div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row row-bottom-margin">
                            <label for="type" class="col-md-3 col-form-label form-control-label"> Type?
                            </label>
                            <div class="col-md-9">
                                <select class="custom-select mr-sm-2" name="type" id="type"
                                        onchange="checkMeeting(this)" required>
                                    <option value="4" selected>Choose</option>
                                    <option value="general">General</option>
                                    <option value="ads">Ads</option>
                                    <option value="meeting">Meeting</option>
                                    <option value="contract">Contract</option>
                                    <option value="vote">Vote</option>
                                </select>
                                <div class="invalid-input" id="type-error"><?php echo $typeErr; ?></div>
                            </div>
                        </div>


                        <div class="form-group row row-bottom-margin">
                            <label for="title" class="col-md-3 col-form-label form-control-label"> Title
                            </label>
                            <div class="col-md-9">
                                <input name="title" id="title" class="form-control" type="text"
                                       required/>
                                <div class="invalid-input" id="title-error"><?php echo $titleErr; ?></div>
                            </div>
                        </div>

                        <div id="meeting-time" style="display: none">
                            <div id="start-time" class="form-group row row-bottom-margin">
                                <label for="start" class="col-md-3 col-form-label form-control-label"> Start time
                                </label>
                                <div class="col-md-9">
                                    <input name="start" id="start" class="form-control" type="datetime-local"
                                    />
                                </div>
                            </div>

                            <div id="end-time" class="form-group row row-bottom-margin">
                                <label for="end" class="col-md-3 col-form-label form-control-label"> End time
                                </label>
                                <div class="col-md-9">
                                    <input name="end" id="end" class="form-control" type="datetime-local"
                                    />
                                    <div class="invalid-input" id="time-error"><?php echo $timeErr; ?></div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row row-bottom-margin">
                            <label for="content" class="col-md-3 col-form-label form-control-label"> Content
                            </label>
                            <div class="col-md-9">
                                <textarea id="content" class="form-control" name="content" rows="3" cols="50"
                                          required></textarea>
                                <div class="invalid-input" id="content-error"><?php echo $contentErr; ?></div>

                            </div>
                        </div>

                        <div class="form-group row row-bottom-margin">
                            <label class="col-md-3 col-form-label form-control-label"></label>
                            <div class="col-md-9">
                                <input type="reset" class="btn btn-secondary" value="Cancel">
                                <input type="submit" name="submit" id="submit" class="btn btn-primary"
                                       value="Post">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>


<?php
include '../partials/footer.php';
?>

