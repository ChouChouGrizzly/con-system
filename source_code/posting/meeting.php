<?php
include '../partials/check_login.php';
include '../partials/header.php';
include '../sql_config/connect_db.php';
/*
Developers:
Sasa Zhang 25117151
Ruoshi Wu 27448724
Jinchen Hu 40080398

*/


$userid = $_SESSION['userid'];
$username = $_SESSION['username'];
$isAdmin = $_SESSION['isAdmin'];
$assid = $_SESSION['assid'];

$public_meetings = $conn->query("select * from post where permit_rule = 0 and posting_type = 'meeting' ORDER BY last_modified_at DESC");

if ($isAdmin == 1) {
    $ass_meetings = $conn->query("select * from post where permit_rule > 0 and permit_rule < 10  and posting_type = 'meeting' ORDER BY last_modified_at DESC");
} else {

    $ass_meetings = $conn->query("select * from post where permit_rule = '$assid' and posting_type = 'meeting' ORDER BY last_modified_at DESC");
}

?>


    <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light border-right sidebar" id="sidebar-wrapper">
        <div class="sidebar-heading"><h4>&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;Posting</h4></div>
        <div class="list-group list-group-flush">
            <a href="post.php" class="list-group-item list-group-item-action bg-light">Post</a>
            <a href="post_list.php" class="list-group-item list-group-item-action bg-light">General</a>
            <a href="ads.php" class="list-group-item list-group-item-action bg-light">Ads</a>
            <a href="meeting.php" class="list-group-item list-group-item-action bg-light">Meeting</a>
            <a href="vote.php" class="list-group-item list-group-item-action bg-light">Vote</a>
            <a href="contracts.php" class="list-group-item list-group-item-action bg-light">Contract</a>
        </div>
    </div>

    <!-- Page Content -->


    <div id="page-content-wrapper">
        <div class="container">
            <!-- content -->


            <div class="accordion" id="accordionExample">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h2 class="mb-0">
                            <button class="btn btn-lg btn-link btn-block text-left" type="button" data-toggle="collapse"
                                    data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Public meetings
                            </button>
                        </h2>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                         data-parent="#accordionExample">
                        <div class="card-body">
                            <div class="container">
                                <div class="list-group">
                                    <?php

                                    if (!is_null($public_meetings) && $public_meetings->num_rows > 0) {
                                        while ($row_every = $public_meetings->fetch_assoc()) {
                                            $p_title = $row_every['title'];
                                            $p_content = $row_every['content'];
                                            $p_p_time = $row_every['posting_time'];
                                            $p_modified_time = $row_every['last_modified_at'];
                                            $p_id = $row_every['post_ID'];
                                            $p_owner_id = $row_every['ownerID'];
                                            $p_type = $row_every['posting_type'];
                                            $p_rule = $row_every['permit_rule'];

                                            $meeting_time = $conn->query("select * from meeting where pid = '$p_id'");
                                            $meeting_row = $meeting_time->fetch_assoc();
                                            $start = $meeting_row['start'];
                                            $end = $meeting_row['end'];

                                            echo "<a class='list-group-item list-group-item-action' 
                                        href='post_form.php?action=post_form&po_id=$p_id'>";
                                            echo '<div class="d-flex w-100 justify-content-between">';
                                            echo '<h4 class="mb-1"><strong> ' . $p_title . '</strong></h4>';
                                            echo '<small>' . $p_p_time . '</small></div>';
                                            echo '<p class="mb-2"><em>' . $p_content . '</em></p>';
                                            echo '<p class="mb-2">Time slot: ' . $start . ' - ' . $end . '</p>';
                                            echo '<small>Last modified: ' . $p_modified_time . '</small></a>';


                                        }//while
                                    } else {
                                        echo '<div class="alert alert-info" role="alert">NO PUBLIC Meetings</div>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h2 class="mb-0">
                            <button class="btn btn-link btn-lg btn-block text-left collapsed" type="button"
                                    data-toggle="collapse"
                                    data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Association meetings
                            </button>
                        </h2>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                            <div class="container">
                                <div class="list-group">
                                    <?php
                                    if (isset($ass_meetings) && $ass_meetings->num_rows > 0) {
                                        while ($row_assoc = $ass_meetings->fetch_assoc()) {
                                            $p_title = $row_assoc['title'];
                                            $p_content = $row_assoc['content'];
                                            $p_p_time = $row_assoc['posting_time'];
                                            $p_modified_time = $row_assoc['last_modified_at'];
                                            $p_id = $row_assoc['post_ID'];
                                            $p_owner_id = $row_assoc['ownerID'];
                                            $p_type = $row_assoc['posting_type'];
                                            $p_rule = $row_assoc['permit_rule'];

                                            $meeting_time = $conn->query("select * from meeting where pid = '$p_id'");
                                            $meeting_row = $meeting_time->fetch_assoc();
                                            $start = $meeting_row['start'];
                                            $end = $meeting_row['end'];

                                            echo "<a class='list-group-item list-group-item-action' 
                                        href='post_form.php?action=post_form&po_id=$p_id'>";
                                            echo '<div class="d-flex w-100 justify-content-between">';
                                            echo '<h4 class="mb-1"><strong> ' . $p_title . '</strong></h4>';
                                            echo '<small>' . $p_p_time . '</small></div>';
                                            echo '<p class="mb-2"><em>' . $p_content . '</em></p>';
                                            echo '<p class="mb-2">Time slot: ' . $start . ' - ' . $end . '</p>';
                                            echo '<small>Last modified: ' . $p_modified_time . '</small></a>';
                                        }
                                    } else {
                                        echo '<div class="alert alert-info" role="alert">NO Association Meetings</div>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                        <h2 class="mb-0">
                            <button class="btn btn-link btn-lg btn-block text-left collapsed" type="button"
                                    data-toggle="collapse"
                                    data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Group meetings
                            </button>
                        </h2>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                         data-parent="#accordionExample">
                        <div class="card-body">
                            <div class="container">
                                <div class="list-group">
                                    <?php

                                    if ($isAdmin == 1) {
                                        $group_meetings = $conn->query("select * from post where permit_rule >=100 and posting_type='meeting'");
                                    } else {
                                        $group_meetings = $conn->query("select * from post join group_member_list on post.permit_rule = group_member_list.groupsID where permit_rule >=100 and posting_type='meeting' and group_member_list.userID = '$userid'");
                                    }
                                    if (!is_null($group_meetings) && $group_meetings->num_rows > 0) {
                                        while ($row = $group_meetings->fetch_assoc()) {
                                            $p_title = $row['title'];
                                            $p_content = $row['content'];
                                            $p_p_time = $row['posting_time'];
                                            $p_modified_time = $row['last_modified_at'];
                                            $p_id = $row['post_ID'];
                                            $p_owner_id = $row['ownerID'];
                                            $p_type = $row['posting_type'];
                                            $p_rule = $row['permit_rule'];

                                            $meeting_time = $conn->query("select * from meeting where pid = '$p_id'");
                                            $meeting_row = $meeting_time->fetch_assoc();
                                            $start = $meeting_row['start'];
                                            $end = $meeting_row['end'];

                                            $groups = $conn->query("select groupName from group_owner where groupsID = '$p_rule'");
                                            $group_row = $groups->fetch_assoc();
                                            $group_name = $group_row['groupName'];
                                            echo "<a class='list-group-item list-group-item-action' 
                                        href='post_form.php?action=post_form&po_id=$p_id'>";
                                            echo '<div class="d-flex w-100 justify-content-between">';
                                            echo '<h4 class="mb-1"><strong> ' . $p_title . '</strong></h4>';
                                            echo '<small>' . $p_p_time . '</small></div>';
                                            echo '<h5 class="mb-1" style="color: cadetblue"> -- From Group '.$group_name.'</h5>';
                                            echo '<p class="mb-2"><em>' . $p_content . '</em></p>';
                                            echo '<p class="mb-2">Time slot: ' . $start . ' - ' . $end . '</p>';
                                            echo '<small>Last modified: ' . $p_modified_time . '</small></a>';

                                        }

                                    } else {
                                        echo '<div class="alert alert-info" role="alert">NO Group Meetings</div>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
include '../partials/footer.php';
?>