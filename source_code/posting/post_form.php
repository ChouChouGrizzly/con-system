<?php

include '../partials/check_login.php';

include '../sql_config/connect_db.php';

/*
Developers:
Sasa Zhang 25117151
Ruoshi Wu 27448724
Jinchen Hu 40080398

*/

$userid = $_SESSION['userid'];
//post_form
$status_child = 0;

$po_id = $_GET['po_id'];
//$_SESSION['po_id'] = $po_id;
//sql
$get_post = $conn->query("select * from post where post_ID='$po_id'");

$post_row = $get_post->fetch_assoc();

$owner_id = $post_row['ownerID'];
$permit_rule = $post_row['permit_rule'];
$type = $post_row['posting_type'];

$get_owner = $conn->query("select userName from user where userId = '$owner_id'");
$get_owner_row = $get_owner->fetch_assoc();
$owner_name = $get_owner_row['userName'];


if (isset($_POST['comment'])) {

    $comment_c = $_POST['post_comment'];

    $result_max = $conn->query("select max(post_ID) as max_id from post");
    $max_row = $result_max->fetch_assoc();
    $new_post_id = $max_row['max_id'] + 1;

    $conn->query("insert into post values('$new_post_id',now(),now(),'$comment_c', '$userid','$po_id', '$permit_rule','comment','comment')");

}

if ($type == "vote" && isset($_POST['vote'])) {
    $opt = (int)$_POST['opt'];
    $conn->query("update vote_list set isVote = '$opt' where userID = '$userid'");
    $vote_result = $conn->query("select * from vote where post_ID = '$po_id'");
    $vote_row = $vote_result->fetch_assoc();
    $sum_agr = $vote_row['agreed'];
    $sum_dis = $vote_row['disagreed'];
    if ($opt == 1) {
        $sum_agr += 1;
        $conn->query("update vote set agreed ='$sum_agr' where post_ID = '$po_id'");
    } else {
        $sum_dis += 1;
        $conn->query("update vote set agreed ='$sum_dis' where post_ID = '$po_id'");
    }
}

include '../partials/header.php';

?>

<!-- topics -->

<?php

if ($conn->error) {
    displayError('Failed to comment/vote');
}

?>

<div class="container">
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4"><?php echo $post_row['title'] ?></h1>
            <p class="lead"><?php echo $post_row['content'] ?></p>
            <small><em>Created by - </em> <?php echo $owner_name ?></small>
            <hr class="my-4">
            <?php

            if ($type == "vote") {
                $vote_state = $conn->query("select * from vote_list where userID = '$userid'");
                $vote_state_row = $vote_state->fetch_assoc();
                $ifvote = $vote_state_row['isVote'];
                if ($ifvote == 0) {
                    //show the vote radio
                    echo '<div class="form-check">';
                    echo '<form method="post">
                    <fieldset class="form-group">
                    <div class="row">
                        <legend class="col-form-label col-sm-2 pt-0">Please vote</legend>
                        <div class="col-sm-10">
                            <div class="form-check">
                              <input class="form-check-input" type="radio" name="opt" id="agree" value="1" checked>
                              <label class="form-check-label" for="agree">
                                Agree
                              </label>
                            </div>
                            <div class="form-check">
                              <input class="form-check-input" type="radio" name="opt" id="disagree" value="2">
                              <label class="form-check-label" for="disagree">
                                Disagree
                              </label>
                            </div>
                        </div>
                    </fieldset>';

                    echo '<div class="form-group row">
                            <div class="col-sm-10">
                            <button type="submit" class="btn btn-primary btn-sm" name="vote" id="vote">Vote</button>
                            </div>
                           </div>';
                    echo '</form><div><hr class="my-4">';
                } else {

                    $vote_result = $conn->query("select * from vote where post_ID = '$po_id'");
                    $vote_row = $vote_result->fetch_assoc();
                    $sum_agr = $vote_row['agreed'];
                    $sum_dis = $vote_row['disagreed'];
                    if ($sum_agr+$sum_dis>0){
                    $agree_pct = round($sum_agr / ($sum_agr+$sum_dis), 2) * 100;
                    $disagree_pct = 100 - $agree_pct;}
                    else{
                        $agree_pct=0;
                        $disagree_pct=0;
                    }

                    echo '<div class="progress" style="width: 300px;">
                          <div class="progress-bar" role="progressbar" style="width:'.$agree_pct.'%" aria-valuenow="'.$agree_pct.'" aria-valuemin="0" aria-valuemax="100">'.$agree_pct.'%</div>
                          <div class="progress-bar bg-danger" role="progressbar" style="width: '.$disagree_pct.'%" aria-valuenow="'.$disagree_pct.'" aria-valuemin="0" aria-valuemax="100">'.$disagree_pct.'%</div>
                        </div><hr class="my-4"><br>';
                }

            }

            ?>

            <?php
            $get_comments = $conn->query("select * from post where  parent_postID='$po_id' order by posting_time");
            if (!is_null($get_comments) && $get_comments->num_rows > 0) {
                while ($row_sin = $get_comments->fetch_assoc()) {
                    $comment_userid = $row_sin['ownerID'];
                    $result = $conn->query("SELECT userName FROM user where userId = '$comment_userid'");
                    $row = $result->fetch_assoc();
                    $comment_username = $row['userName'];
                    echo '<i class="fas fa-user-ninja"> ' . $comment_username . '</i>';

                    echo "<div><p>" . $row_sin['content'] . "</p></div>";
                    echo '<hr class="my-4">';

                }
            } else {
                echo '<div class="alert alert-warning" role="alert">
                        No Comments Found!
                      </div>';
            }
            ?>

            <div class="container">
                <form id="comment-post" class="" method="post"
                      action="">

                    <div class="form-group">
                        <label for="post_comment"> Write your comment
                        </label>
                        <textarea id="post_comment" class="form-control" name="post_comment" rows="2" cols="10"
                                  placeholder="Your comment" required></textarea
                    </div>
                    <br>
                    <div class="form-group row">
                        <div class="col-sm-10">
                            <input type="reset" class="btn btn-secondary btn-sm" value="Cancel">
                            <button type="submit" class="btn btn-primary btn-sm" name="comment" id="comment"
                                    value="comment">
                                comment
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- //content -->


<?php
include '../partials/footer.php';
?>


