Course Name: COMP353 / FALL 2020 / Section F

Instructor: Dr. Bipin C Desai

Team ID: COMP_353_group_7



====================Group members====================
1. Leader: Sasa ZHANG (25117151)
           sasa_zha@encs.concordia.ca

2. Member: Ruoshi WU (27448724)
           w_ruoshi@encs.concordia.ca

3. Member: Jinchen HU (40080398)
           h_jinch@encs.concordia.ca

4. Member: Ziqian LI (27795327)
           l_ziqian@encs.concordia.ca



====================List of files submitted====================
1. README.txt
2. P1_report.pdf
3. P1_source_code.zip



====================ENCS server information====================
1. The group account:                rzc353_2
2. The MYSQL username:               rzc353_2
3. The name of the MYSQL server:     rzc353.encs.concordia.ca
4. The name of the database:         rzc353_2
5. The password for the database:    H75KFg
6. The user ID for web access:       rzc353_2
7. The password for web access:      H75KFg
8. The base URL for the web page:    https://rzc353.encs.concordia.ca/



====================Remote installation and setup on ENCS Linux server====================
1. MySQL version 8.0.18 and PHP version 7.2.11

2. Using `PuTTY` to connect to the ENCS Linux server by using your ENCS account (Done by us already, you do not need to do anything)

3. Change your current working directory to `/www/groups/r/rz_comp353_2/sql_config` (Done by us already, you do not need to do anything)

4. Connect to MySQL by using the following command `mysql -h rzc353.encs.concordia.ca -u rzc353_2 -p rzc353_2` (Done by us already, you do not need to do anything)

5. Enter the password `H75KFg` (Done by us already, you do not need to do anything)

6. Execute by entering the command `source dump_data.sql` after connecting to MySQL for installing the database of our CON System (Done by us already, you do not need to do anything)

7. Enter the URL in the browser `https://rzc353.encs.concordia.ca/partials/login.php` for entering the login page

8. Enter the URL in the browser `https://rzc353.encs.concordia.ca/index.php` for entering the home page

Note: we also include a detailed README section in our report `P1_report.pdf`.



====================Local installation and setup on the local machine====================
1. MySQL version 8.0.18 and PHP version 7.2.11

2. Open up the project file with IDE (e.g., PhpStorm)

3. Set the project folder `comp353-cns` as sources root

4. Configure server name, port, database username, password in `config.php`

5. Set up database connection in `\sql_config\connect_db.php`

6. Run `\sql_config\dump_data.sql` to load initial data into database

Note: we also include a detailed README section in our report `P1_report.pdf`.



====================Test accounts====================
1. System admin (username: admin; password: admin)

2. Association admin (username: nathen; password: user)

3. Association admin (username: jing; password: user)

4. Normal user (username: allison; password: user)