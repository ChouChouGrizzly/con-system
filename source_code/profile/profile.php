<?php

include '../partials/check_login.php';
include '../partials/header.php';
include '../sql_config/connect_db.php';
/*
Developers:
Sasa Zhang 25117151
Ruoshi Wu 27448724
Jinchen Hu 40080398

*/
$nameErr = "";
$memberId = $_SESSION['userid'];
$memberName = $_SESSION['username'];
$memberEmail = $_SESSION['email'];
$memberRole = $_SESSION['isAdmin'] ? "Admin" : "Member";
$memberDOB = $_SESSION['DOB'];
$memberAdd = is_null($_SESSION['address']) ? "Not Set" : $_SESSION['address'];

?>


<div class="card " id="profile-card">
    <div class="card-body mx-auto">
        <h5 class="card-title">Member Profile</h5>
        <p><strong>Member ID</strong>: <?php echo $memberId ?></p>
        <p><strong>Member Name</strong>: <?php echo $memberName ?></p>
        <p><strong>Role</strong>: <?php echo $memberRole ?></p>
        <p><strong>Date of birth</strong>: <?php echo $memberDOB ?></p>
        <p><strong>Email</strong>: <?php echo $memberEmail ?></p>
<!--        <p><strong>Condo Number</strong>: --><?php //echo $condoNo ?><!--</p>-->
        <p><strong>Address</strong>: <?php echo $memberAdd ?></p>
        <hr class="4">
        <a class="btn btn-sm btn-primary" href="profile_edit.php" role="button">Edit</a><br><br>

    <div class="card">
        <div class="card-header">Occupancy</div>
        <table class="table table-hover table-striped ">
            <thead>
            <tr>
                <th scope="col">Building Number</th>
                <th scope="col">Condo Number</th>
                <th scope="col">Percentage Of Share</th>
            </tr>
            </thead>
            <tbody>
    <?php
    $sql1 = 'SELECT * FROM condo_unit WHERE ownerID=' . $_SESSION['userid'];
    $result = $conn->query($sql1);
    if($result->num_rows != 0){
        while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $condoNo = $row['condoID'];
            $buildingNo=$row['buildingID'];
            $percent=$row['percentage_share'];
            echo "<tr><td>".$buildingNo."</td><td>".$condoNo."</td><td>".$percent."</td></tr>";
        }
    }else{
        echo "No condo registered under this user.";
    }


    ?>
            </tbody>
        </table>
    </div>
</div>
    </div>
</div>

<?php
include '../partials/footer.php';
?>