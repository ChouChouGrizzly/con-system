<?php

include '../partials/check_login.php';
include '../partials/header.php';
include '../sql_config/connect_db.php';
/*
Developers:
Sasa Zhang 25117151
Ruoshi Wu 27448724
Jinchen Hu 40080398

*/
$nameErr = "";
$memberId = $_SESSION['userid'];
$memberName = $_SESSION['username'];
$memberEmail = $_SESSION['email'];
$memberRole = $_SESSION['isAdmin'] ? "Admin" : "Member";
$memberDOB = $_SESSION['DOB'];
$memberAdd = is_null($_SESSION['address']) ? "Not Set" : $_SESSION['address'];


if (isset($_POST['name'])) {
    $memberName = $_POST['name'];
    $_SESSION['username'] = $memberName;

    $sql = "SELECT COUNT(*) FROM user WHERE userName = " . $memberName;
    if ($conn->query($sql) != 0) {
        $nameErr = "Sorry ... username already taken";
    } else {
        $conn->query("UPDATE user SET userName ='$memberName' WHERE userId='$memberId'");

    }
}

$success = false;
if (isset($_POST['password'])) {
    $memberPwd = $_POST['password'];
    $conn->query("UPDATE user SET pwd='$memberPwd' WHERE userId='$memberId'");
    $success = true;
}

//if (isset($_POST['email'])) {
//    $memberEmail = $_POST['email'];
//    $conn->query("UPDATE Member SET InternalEmail='$memberEmail' WHERE MemberId='$memberId'");
//
//}
if (isset($_POST['dob'])) {
    $memberDOB = $_POST['dob'];
    $conn->query("UPDATE user SET DOB='$memberDOB' WHERE userId='$memberId'");
    $success = true;

}
$success = false;
if (isset($_POST['address'])) {
    $memberAdd = $_POST['address'];
    $conn->query("UPDATE user SET Address='$memberAdd' WHERE userId='$memberId'");
    $success = true;
}

if ($nameErr == "" && $success) {
    displaySuccess("Information Saved Successfully");
    echo '<script>
            //alert("Save Successfully！");
            location.href = "profile.php";
          </script>;';
}

$conn->close();

?>


<?php
if (strtolower($_SESSION['username']) == 'admin') {
    displayWarning("It's recommended to change your username and password in the first login");
}
?>

    <div class="container">
        <div class="card">
            <div class="card-header">
                <h4 class="mb-0">User Information</h4>
            </div>
            <div class="card-body">
                <form id="user_info" class="" method="post"
                      action="">
                    <div class="form-group row row-bottom-margin">
                        <label for="userid" class="col-md-3 col-form-label form-control-label">Member ID</label>
                        <div class="col-md-9">
                            <input name="userid" id="userid" class="form-control" type="text"
                                   placeholder="<?php echo $memberId ?>"
                                   readonly>
                        </div>
                    </div>
                    <div class="form-group row row-bottom-margin">
                        <label for="name" class="col-md-3 col-form-label form-control-label">Name</label>
                        <div class="col-md-9">
                            <input id="name" name="name" class="form-control" type="text" onblur="checkNewName()"
                                   value="<?php echo $memberName ?>"
                            >
                            <div class="invalid-input" id="user-error"><?php echo $nameErr ?></div>
                        </div>
                    </div>
                    <div class="form-group row row-bottom-margin">
                        <label for="password" class="col-md-3 col-form-label form-control-label">New Password</label>
                        <div class="col-md-9">
                            <input name="password" id="password" class="form-control" type="password"
                                   onblur="checkNewPassword()">
                            <div class="invalid-input" id="psw-error"></div>
                        </div>
                    </div>
                    <div class="form-group row row-bottom-margin">
                        <label for="password_again" class="col-md-3 col-form-label form-control-label">Confirm
                            Password</label>
                        <div class="col-md-9">
                            <input name="password_again" id="password_again" class="form-control" type="password"
                                   onblur="confirmPsw()">
                            <div class="invalid-input" id="psw-confirm-error"></div>
                        </div>
                    </div>

                    <div class="form-group row row-bottom-margin">
                        <label for="dob" class="col-md-3 col-form-label form-control-label">Date of Birth</label>
                        <div class="col-md-9">
                            <input id="dob" name="dob" class="form-control" type="date"
                                   value="<?php echo $memberDOB ?>">
                        </div>
                    </div>
                    <div class="form-group row row-bottom-margin">
                        <label for="address" class="col-md-3 col-form-label form-control-label"> Address
                        </label>
                        <div class="col-md-9">
                            <input name="address" id="address" class="form-control" type="text"
                                   value="<?php echo $memberAdd ?>">
                        </div>
                    </div>
                    <div class="form-group row row-bottom-margin">
                        <label class="col-md-3 col-form-label form-control-label"></label>
                        <div class="col-md-9">
                            <input type="reset" class="btn btn-secondary" value="Cancel">
                            <input type="submit" name="update" onclick="gotoprofile()" id="update"
                                   class="btn btn-primary" value="Save"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


<?php
include '../partials/footer.php';
?>