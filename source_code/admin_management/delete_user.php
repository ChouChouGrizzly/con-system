<?php
include '../partials/check_login.php';
include '../partials/header.php';
include '../sql_config/connect_db.php';

/*
Developers:
Sasa Zhang 25117151
Ruoshi Wu 27448724
Jinchen Hu 40080398

*/

//userid to query all messages from/to the user
$userid = $_SESSION['userid'];
$username = $_SESSION['username'];

//system admin
if ($userid == 100) {
//find your associationID from con_association
    $sql_association = "select associationID, assoc_adminID from condo_assoc ";
//execute query
    $result_association = $conn->query($sql_association);
//num of result
    $num = $result_association->num_rows;
//check if user is exist
    if ($num) {
        //$row = mysqli_fetch_array($result);

    } else {
        displayError("Error: find associationID function " . $sql_association . "<br>" . mysqli_error($conn));
    }
}//if userid==100
//execpt system admin
if ($_SESSION['isAdmin'] == 2 && $userid != 100) {
    //find your associationID from con_association
    $sql_association = "select * from condo_assoc where assoc_adminID='$userid' ";
//execute query
    $result_association = $conn->query($sql_association);
//num of result
    $num = $result_association->num_rows;
//check if user is exist
    if ($num) {
        //$row = mysqli_fetch_array($result);

    } else {
        displayError( "Error: find associationID function 2 " . $sql_association . "<br>" . mysqli_error($conn));
    }
}

//check refresh button
if (isset($_GET['operator']) && $_GET['operator'] == 'refresh') {
    header("location:delete_user.php");
    exit;
}

//check manage association
$manage_assoc_status = 0;
if (isset($_GET['operator']) && $_GET['operator'] == 'manage_assoc') {
    $manage_assoc_status = 1;
    $assoc_id = $_GET['assoc'];
    $_SESSION['assoc_id'] = $assoc_id;

    $admin_id = $_GET['admin_id'];
    $_SESSION['admin_id'] = $admin_id;
    //$stdby_admin_id = $_GET['stdby_admin_id'];
    //$_SESSION['stdby_admin_id'] = $stdby_admin_id;

    //sql
    $sql_assoc_user_id = "select userID from assoc_member_list where associationID='$assoc_id' and userID !='$userid' ";
    $result_user_id = $conn->query($sql_assoc_user_id);
    //num
    $num_user = $result_user_id->num_rows;
    if ($num_user) {
        $manage_assoc_status = 1;
    } else {
        displayError( "Error: find association userID function " . $sql_assoc_user_id . "<br>" . mysqli_error($conn));
    }

}
//check delete member
$delete_member_status = 0;
if (isset($_GET['operator']) && $_GET['operator'] == 'delete_member') {
    $manage_assoc_id = $_GET['assoc_id'];
    $manage_user_id = $_GET['del_user_id'];
    $manage_condo_id = $_GET['condo_id'];
    //sql
    $sql_delete_member = "delete from assoc_member_list where userID = '$manage_user_id' and associationID='$manage_assoc_id' ";
    if (mysqli_query($conn, $sql_delete_member)) {
        $conn->query("UPDATE condo_unit SET ownerID = NULL where condoID = '$manage_condo_id'");
        //update member isAdmin = 0
        $sql_user_isAdmin = "update user set isAdmin=0 where userId='$manage_user_id' ";
        if (mysqli_query($conn, $sql_user_isAdmin)) {
            $delete_member_status = 1;
        } else {
            displayError( "Error: delete member from Association 2 " . $sql_delete_member . "<br>" . mysqli_error($conn));
        }
    } else {
        displayError( "Error: delete member from Association " . $sql_delete_member . "<br>" . mysqli_error($conn));
    }
}

//add member to association
$add_member_status = 0;
if (isset($_POST['submit']) && $_POST['submit'] == 'add_member') {

    $add_member_id = $_POST['add_member_id'];
    $add_assco_id = $_SESSION['assoc_id'];
    //sql
    $sql_add_member = "insert into assoc_member_list values('$add_assco_id','$add_member_id')";
    if (mysqli_query($conn, $sql_add_member)) {
        $add_member_status = 1;
    } else {
        displayError( "Error: add member into Association " . $sql_add_member . "<br>" . mysqli_error($conn));
    }

}
////upgrade admin
//$upgrade_admin_status = 0;
//if(isset($_POST['submit']) && $_POST['submit'] == 'upgrade_admin'){
//    $upgrade_assoc_id = $_SESSION['assoc_id'];
//    $upgrade_user_id = $_POST['upgrade_admin_id'];
//    //check the user is belong to the association
//    $sql_check_user = "select associationID from ca_mb_list where userID='$upgrade_user_id' and associationID='$upgrade_assoc_id'";
//    $result_check = $conn->query($sql_check_user);
//    $num_check = $result_check->num_rows;
//    if($num_check){
//        //sql
//        $sql_upgrade_admin = "update con_association set assoc_adminID = '$upgrade_user_id' where associationID='$upgrade_assoc_id' ";
//        //execute
//        if(mysqli_query($conn, $sql_upgrade_admin)){
//
//            //set new admin, then degrade old admin
//            $sql_new_admin = "update user set isAdmin=1 where userId='$upgrade_user_id'";
//            if(mysqli_query($conn,$sql_new_admin)){
//                //degrade old admin
//                $old_admin = $_SESSION['admin_id'];
//                //sql
//                $sql_degrade_admin = "update user set isAdmin=0 where userId='$old_admin' ";
//                if(mysqli_query($conn,$sql_degrade_admin)){
//                    $upgrade_admin_status = 1;
//                }else{
//                    echo "Error: upgrade member to admin into Association 3 " . $sql_degrade_admin . "<br>" . mysqli_error($conn);
//                }
//            }else{
//                echo "Error: upgrade member to admin into Association 2 " . $sql_new_admin . "<br>" . mysqli_error($conn);
//            }
//        }else{
//            echo "Error: upgrade member to admin into Association " . $sql_upgrade_admin . "<br>" . mysqli_error($conn);
//        }
//    }else{
//        $upgrade_admin_status = 3;
//    }
//
//}
////upgrade stdby_admin
//$upgrade_stdby_admin_status = 0;
//if(isset($_POST['submit']) && $_POST['submit'] == 'upgrade_stdby_admin'){
//    $upgrade_assoc_id = $_SESSION['assoc_id'];
//    $upgrade_user_id = $_POST['upgrade_admin_id'];
//    //case null
//    if($upgrade_user_id == 'null'){
//        //sql
//        $sql_upgrade_admin = "update con_association set stdby_adminID = null where associationID='$upgrade_assoc_id' ";
//        //execute
//        if(mysqli_query($conn, $sql_upgrade_admin)){
//            $upgrade_stdby_admin_status = 1;
//            //set new _stdby_admin = null, then degrade old admin
//            //degrade old admin
//            $old_stdby_admin = $_SESSION['stdby_admin_id'];
//            //sql
//            $sql_degrade_stdby_admin = "update user set isAdmin=0 where userId='$old_stdby_admin' ";
//            if(mysqli_query($conn,$sql_degrade_stdby_admin)){
//                $upgrade_stdby_admin_status = 1;
//            }else{
//                echo "Error: upgrade member to stdby admin into Association 3 " . $sql_degrade_stdby_admin . "<br>" . mysqli_error($conn);
//            }
//        }else{
//            echo "Error: upgrade member to stdby admin into Association " . $sql_upgrade_admin . "<br>" . mysqli_error($conn);
//        }
//    }else{
//        //check the user is belong to the association
//        $sql_check_user = "select associationID from ca_mb_list where userID='$upgrade_user_id' and associationID='$upgrade_assoc_id'";
//        $result_check = $conn->query($sql_check_user);
//        $num_check = $result_check->num_rows;
//        if($num_check){
//            //sql
//            $sql_upgrade_admin = "update con_association set stdby_adminID = '$upgrade_user_id' where associationID='$upgrade_assoc_id' ";
//            //execute
//            if(mysqli_query($conn, $sql_upgrade_admin)){
//                //$upgrade_stdby_admin_status = 1;
//                //set new _stdby_admin, then degrade old admin
//                $sql_new_stdby_admin = "update user set isAdmin=1 where userId='$upgrade_user_id'";
//                if(mysqli_query($conn,$sql_new_stdby_admin)){
//                    //degrade old admin
//                    $old_stdby_admin = $_SESSION['stdby_admin_id'];
//                    //sql
//                    $sql_degrade_stdby_admin = "update user set isAdmin=0 where userId='$old_stdby_admin' ";
//                    if(mysqli_query($conn,$sql_degrade_stdby_admin)){
//                        $upgrade_stdby_admin_status = 1;
//                    }else{
//                        echo "Error: upgrade member to stdby admin into Association 3 " . $sql_degrade_stdby_admin . "<br>" . mysqli_error($conn);
//                    }
//                }else{
//                    echo "Error: upgrade member to stdby admin into Association 2 " . $sql_new_admin . "<br>" . mysqli_error($conn);
//                }
//
//
//            }else{
//                echo "Error: upgrade member to stdby admin into Association " . $sql_upgrade_admin . "<br>" . mysqli_error($conn);
//            }
//        }else{
//            $upgrade_stdby_admin_status = 3;
//        }
//    }
//
//
//
//}


//include '../partials/header.php';
?>
    <div class="d-flex" id="wrapper">

        <!-- Sidebar -->
        <div class="bg-light border-right sidebar" id="sidebar-wrapper">
            <div class="sidebar-heading"><h4>&nbsp; &nbsp;&nbsp; &nbsp;Admin <br>Management</h4></div>
            <div class="list-group list-group-flush">
                <a href="create_user.php" class="list-group-item list-group-item-action bg-light">Create Members</a>
<!--                <a href="edit_user.php" class="list-group-item list-group-item-action bg-light">Edit Members</a>-->
                <a href="delete_user.php" class="list-group-item list-group-item-action bg-light">Delete
                    Members</a>
                <?php
                if($_SESSION['userid']==100){
                    echo'<a href="assign_assoc_admin.php" class="list-group-item list-group-item-action bg-light">Assign Admin
            </a>';} ?>

            </div>
        </div>

        <div id="page-content-wrapper">
            <div class="container">
                <div class="card">
                    <div class="card-header">
                        <h4 class="mb-0">Delete Users</h4>
                    </div>
                    <div class="card-body">
                        <div class="container-fluid yo">
                            <br>
                            <br>
                            <table style="width: auto" class="table table-striped table-hover ">
                                <thead>
                                <tr>
                                    <th style="align-content: center">Association ID</th>
                                    <th style="align-content: center">Admin ID</th>
                                    <th style="align-content: center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                //find your all groups from group_leader
                                while ($row = mysqli_fetch_array($result_association)) {
                                    //find each group info
                                    $get_assoc_id = $row['associationID'];
                                    $get_assoc_admin_id = $row['assoc_adminID'];
                                    //echo
                                    echo "<tr><td>" . $get_assoc_id . "</td><td>" . $get_assoc_admin_id . "</td><td>" .

                                        "<a href='delete_user.php?operator=manage_assoc&assoc=$get_assoc_id&admin_id=$get_assoc_admin_id' > 
                                  <button type='button' class='btn btn-info btn-sm' name='manage_assoc' >Check Members</button>
                                </a> " . "</td><tr>";

                                }

                                ?>
                                </tbody>
                            </table>
                        </div>

                        <hr class="my-4">

                        <!-- check create status-->
                        <?php
                        if ($manage_assoc_status == 1) {
//    echo "<h4 style='align-content: center'>Association id " . $_SESSION['assoc_id'] . "</h4>";
//    echo '<form action=""  method="post">
//                        <div class="input-group input-group-sm">
//                       <input type="text" class="form-control" id="add_member_id" name="add_member_id" placeholder="please enter userID "/>
//                        <div class="input-group-btn">
//                       <button type="submit" class="btn btn-primary btn-sm" name="submit" value="add_member">add member</button>
//                     </div>
//                     </div>
//                     <div class="input-group input-group-sm">
//                       <input type="text" class="form-control" id="upgrade_admin_id" name="upgrade_admin_id" placeholder="please enter userID to upgrade admin or stdby admin "/>
//                        <div class="input-group-btn">
//                       <button type="submit" class="btn btn-success btn-sm" name="submit" value="upgrade_admin">upgrade admin</button>
//                       <button type="submit" class="btn btn-info btn-sm" name="submit" value="upgrade_stdby_admin">upgrade stdby admin</button>
//                     </div>
//                     </div>
//                     </form>';


                            echo '
                <table style="width: auto" class="table table-striped table-hover "> ';
                            echo '
                <thead>
                <tr>
                <th style="align-content: center">Member ID</th>
                <th style="align-content: center">Member Name</th>
                <th style="align-content: center">Building ID</th>
                <th style="align-content: center">Condo ID</th>
                <th style="align-content: center">Is Active</th>
                <th style="align-content: center">Action</th>
                </tr>
                </thead>
                <tbody>
                ';
                            while ($row_user = mysqli_fetch_array($result_user_id)) {
                                $manage_user_id = $row_user['userID'];

                                $member = $conn->query("SELECT userName, isActive FROM user where userId = '$manage_user_id'");
                                $memberrow = $member->fetch_assoc();
                                $condo_unit = $conn->query("SELECT * FROM condo_unit where ownerID = '$manage_user_id'");
                                $condo_row = $condo_unit->fetch_assoc();
                                $building_id = $condo_row['buildingID'];
                                $condo_id = $condo_row['condoID'];
                                $memberName = $memberrow['userName'];
                                $isActive = $memberrow['isActive'] == 1 ? "YES" : "NO";
                                $result_assoc_id = $_SESSION['assoc_id'];
                                $result_admin_id = $_SESSION['admin_id'];
                                $result_stdby_admin_id = $_SESSION['stdby_admin_id'];
                                if ($manage_user_id == $result_admin_id) {
                                    $result_status = 'admin';
                                } elseif ($manage_user_id == $result_stdby_admin_id) {
                                    $result_status = 'stdby admin';
                                } else {
                                    $result_status = 'member';
                                }

                                echo "<tr><td>" . $manage_user_id . "</td><td>" . $memberName . "</td><td>" . $building_id . "</td><td>" . $condo_id . "</td><td>" . $isActive . "</td><td>" . "<a href='delete_user.php?operator=delete_member&assoc_id=$result_assoc_id&condo_id=$condo_id&del_user_id=$manage_user_id' > 
                                  <button type='button' class='btn btn-danger btn-sm' name='delete_member'>delete</button>
                                </a> " . "</td></tr>";

                            }//while

                            echo '</tbody></table>';

                        }//if

                        if ($delete_member_status == 1) {
//                            echo '<div class="alert alert-warning alert-dismissible fade show" role="alert">
//                                    <strong>Successfully</strong> Deleted a Member
//                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
//                                        <span aria-hidden="true">&times;</span>
//                                    </button>
//                                  </div>';
                            displaySuccess("Successfully deleted a Member.");
                        }

                        //                        if ($add_member_status == 1) {
                        //                            echo "add member into association success!";
                        //                        }
                        //if ($upgrade_admin_status == 1) {
                        //    echo "upgrade admin success!";
                        //}
                        //if ($upgrade_admin_status == 3) {
                        //    echo "UserID is incorrect! upgrade admin fail!";
                        //}
                        //if ($upgrade_stdby_admin_status == 1) {
                        //    echo "upgrade stdby admin success!";
                        //}
                        //if ($upgrade_stdby_admin_status == 3) {
                        //    echo "UserID is incorrect! upgrade stdby admin fail!";
                        //}

                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
include '../partials/footer.php';
?>