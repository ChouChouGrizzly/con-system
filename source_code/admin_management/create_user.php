<?php
include '../partials/check_login.php';
include '../partials/header.php';
include '../sql_config/connect_db.php';

/*
Developers:
Sasa Zhang 25117151
Ruoshi Wu 27448724
Jinchen Hu 40080398

*/

$saved = 0;
$nameErr = $buildingErr = $condoErr = $emailErr = $assErr = "";
if (isset($_POST['save-user'])) {
    //$userid = $_POST['userid'];
    $username = $_POST['name'];
    $password = $_POST['password'];
    $buildingNo = $_POST['building-no'];
    $condNo = $_POST['condo-no'];
    $email = $_POST['email'];
    $dob = $_POST['dob'];
    $address = $_POST['address'];

    if (!empty($username)) {
        // check if the name exists in the database
        $sql1 = "SELECT COUNT(*) as total FROM user where userName = '$username'";
        $result = $conn->query($sql1) or die($conn->error);
        $total = $result->fetch_assoc();
        if ($total['total'] >= 1) {
            $nameErr = 'The user name has been taken';
        }
    }

    if (!empty($email)) {
        // check if the name exists in the database
        $sql1 = "SELECT COUNT(*) as total FROM user where Email = '$email' ";
        $result = $conn->query($sql1) or die($conn->error);
        $total = $result->fetch_assoc();
        if ($total['total'] >= 1) {
            $emailErr = 'The email has been taken';
        }
    }

    if (!is_null($buildingNo) && !is_null($condNo)) {
        // check if the id exists in the database
        $sql1 = "SELECT *  FROM condo_unit where buildingID = '$buildingNo' AND condoID= '$condNo'";
        $result = $conn->query($sql1) or die($conn->error);
        if (empty($result) || $result->num_rows != 1) {
            $buildingErr = 'The building does not have this condo';
        } else {
            $condo = $result->fetch_assoc();
            if(!is_null($condo['ownerID'])){
                $buildingErr = 'This condo has been registered';
            }
        }
    }

    // save the data to the database

    if ($nameErr == "" && $buildingErr == "" && $condoErr == "" && $emailErr == "") {


        // get the adminId with building number
        $sql3 = "SELECT * FROM building where BuildingId= '$buildingNo'";
        $result = $conn->query($sql3) or die($conn->error);
        $building = $result->fetch_assoc();
        $assoID = $building['associationID'];

        $sql2 = "INSERT INTO user VALUES (NULL,'$username','$password' , '$address', 0,'$email','$dob',1)";
        $insertUser = $conn->query($sql2) or die($conn->error);
        $userid = $conn -> insert_id;

        $sql4 = "UPDATE condo_unit SET ownerID = '$userid' where condoID = '$condNo' and buildingID = '$buildingNo'";
        $conn->query($sql4) or die($conn->error);
        $conn->query("INSERT INTO assoc_member_list VALUES ('$assoID', '$userid')");
        $saved = 1;
    }

    $conn->close();
}

?>
<script src="../js/helper.js"></script>

<div class="d-flex" id="wrapper">

    <div class="bg-light border-right sidebar" id="sidebar-wrapper">
        <div class="sidebar-heading"><h4>&nbsp; &nbsp;&nbsp; &nbsp;Admin <br>Management</h4></div>
        <div class="list-group list-group-flush">
            <a href="create_user.php" class="list-group-item list-group-item-action bg-light">Create Members</a>
<!--            <a href="edit_user.php" class="list-group-item list-group-item-action bg-light">Edit Members</a>-->
            <a href="delete_user.php" class="list-group-item list-group-item-action bg-light">Delete
                Members</a>
            <?php
            if($_SESSION['userid']==100){
            echo'<a href="assign_assoc_admin.php" class="list-group-item list-group-item-action bg-light">Assign Admin
            </a>';} ?>
        </div>
    </div>

    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-0">Member Creation</h4>
                </div>
                <div class="card-body">
                    <form id=create-members class="" method="post"
                          action="">
                        <div class="form-group row row-bottom-margin">
                            <label for="name" class="col-md-3 col-form-label form-control-label">Name</label>
                            <div class="col-md-9">
                                <input id="name" name="name" class="form-control" type="text" onblur="checkNewName()"

                                       required/>
                                <div class="invalid-input" id="user-error"><?php echo $nameErr; ?></div>
                            </div>
                        </div>
                        <div class="form-group row row-bottom-margin">
                            <label for="password" class="col-md-3 col-form-label form-control-label">Password</label>
                            <div class="col-md-9">
                                <input name="password" id="password" class="form-control" type="password"
                                       onblur="checkNewPassword()" required/>
                                <div class="invalid-input" id="psw-error"></div>
                            </div>
                        </div>

                        <div class="form-group row row-bottom-margin">
                            <label for="building-no" class="col-md-3 col-form-label form-control-label">Building
                                Number</label>
                            <div class="col-md-9">
                                <input name="building-no" id="building-no" class="form-control" type="number"
                                       required/>
                                <div class="invalid-input" id="building-confirm-error"><?php echo $buildingErr; ?></div>
                            </div>
                        </div>

                        <div class="form-group row row-bottom-margin">
                            <label for="condo-no" class="col-md-3 col-form-label form-control-label">Condo
                                Number</label>
                            <div class="col-md-9">
                                <input name="condo-no" id="condo-no" class="form-control" type="number"
                                       required/>
                                <div class="invalid-input" id="condo-confirm-error"><?php echo $condoErr; ?></div>
                            </div>
                        </div>

                        <div class="form-group row row-bottom-margin">
                            <label for="email" class="col-md-3 col-form-label form-control-label">Email</label>
                            <div class="col-md-9">
                                <input name="email" id="email" class="form-control" type="email"
                                       onblur="checkEmail()" required/>
                                <div class="invalid-input" id="email-error"><?php echo $emailErr; ?></div>
                            </div>
                        </div>
                        <div class="form-group row row-bottom-margin">
                            <label for="dob" class="col-md-3 col-form-label form-control-label">Date of Birth</label>
                            <div class="col-md-9">
                                <input id="dob" name="dob" class="form-control" type="date" value="1970-01-01">
                            </div>
                        </div>

                        <div class="form-group row row-bottom-margin">
                            <label for="address" class="col-md-3 col-form-label form-control-label"> Address
                            </label>
                            <div class="col-md-9">
                                <input name="address" id="address" class="form-control" type="text"
                                >
                            </div>
                        </div>
                        <div class="form-group row row-bottom-margin">
                            <label class="col-md-3 col-form-label form-control-label"></label>
                            <div class="col-md-9">
                                <input type="reset" class="btn btn-secondary" value="Cancel">
                                <input type="submit" name="save-user" id="save-user" class="btn btn-primary"
                                       value="Submit">
                            </div>
                        </div>
                    </form>
                </div>

                <?php
                if ($saved == 1) {
//                    echo '<div class="alert alert-warning alert-dismissible fade show" role="alert">
//                                    <strong>Successfully</strong> saved a Member
//                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
//                                        <span aria-hidden="true">&times;</span>
//                                    </button>
//                                  </div>';
                    displaySuccess("Successfully saved a Member.");
                }
                ?>
            </div>
        </div>
    </div>

</div>


<?php
include '../partials/footer.php';
?>
