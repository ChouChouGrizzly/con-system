<?php
include '../partials/check_login.php';
include '../partials/header.php';
include '../sql_config/connect_db.php';

/*
Developers:
Sasa Zhang 25117151
Ruoshi Wu 27448724
Jinchen Hu 40080398

*/

$userIdErr = $assocIdErr = $inputUser = $inputAssoc = "";

if (isset($_POST['summit'])) {
    if (!empty($_POST['userId'])) {
        $inputUser = $_POST['userId'];
        $checkUser = mysqli_query($conn, "SELECT * from user WHERE userId='$inputUser'");
        $count = mysqli_num_rows($checkUser);
        if ($count == 0) {
            $userIdErr = "This user Id is not existed.";

        }
        // check if this user belong to the selected assoc ID
        if (!empty($_POST['assocId'])) {
            $inputAssoc = $_POST['assocId'];
            $checkAssoc = mysqli_query($conn, "SELECT * FROM assoc_member_list WHERE userID='$inputUser' AND associationID ='$inputAssoc'");
            $count1 = mysqli_num_rows($checkAssoc);
            if ($count1 == 0) {
                $assocIdErr = "User ID is not registered under this condo association ID";
            }
            // check if he is already the Admin for this condo association
            $check=mysqli_query($conn,"SELECT * from condo_assoc WHERE assoc_adminID='$inputUser' AND associationID='$inputAssoc'");
            $count2=mysqli_num_rows($check);
            if($count2!=0){
                $userIdErr = "This user Id is already an Admin for this condo association.";

            }
            // Get info details for this condo association
            $assocDetails=mysqli_query($conn,"SELECT * FROM condo_assoc WHERE associationID='$inputAssoc' ");
            $result=mysqli_fetch_array($assocDetails);
            $assoc_balance=$result['assoc_balance'];
            $budget=$result['budget'];
            $cost_sm_storage=$result['cost_sm_storage'];
            $cost_sm_parking=$result['cost_sm_parking'];
            $cost_sm_condo=$result['cost_sm_condo'];



        }

    }

    $updateAdminStatus = 0;


    if (($userIdErr == "") && ($assocIdErr == "") && (!empty($_POST['userId'])) && (!empty($_POST['assocId']))) {
        $updateAdmin = "UPDATE user SET isAdmin=2 WHERE userId='$inputUser'";
        $updateAdmin2 = "INSERT INTO condo_assoc VALUES ('$inputAssoc','$assoc_balance','$inputUser','$budget','$cost_sm_storage','$cost_sm_parking','$cost_sm_condo')";
        if ((mysqli_query($conn, $updateAdmin)) && (mysqli_query($conn, $updateAdmin2))) {
            $updateAdminStatus = 1;
        } else {
            displayError( "Error: Update Admin " . $updateAdmin . "<br>" . mysqli_error($conn));
        }
    }

}


?>

<script src="../js/helper.js"></script>

<div class="d-flex" id="wrapper">

    <div class="bg-light border-right sidebar" id="sidebar-wrapper">
        <div class="sidebar-heading"><h4>&nbsp; &nbsp;&nbsp; &nbsp;Admin <br>Management</h4></div>
        <div class="list-group list-group-flush">
            <a href="create_user.php" class="list-group-item list-group-item-action bg-light">Create Members</a>
            <!--            <a href="edit_user.php" class="list-group-item list-group-item-action bg-light">Edit Members</a>-->
            <a href="delete_user.php" class="list-group-item list-group-item-action bg-light">Delete
                Members</a>
            <a href="assign_assoc_admin.php" class="list-group-item list-group-item-action bg-light">Assign Admin
            </a>
        </div>
    </div>

    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-0">Assign admin for condo association</h4>
                </div>
                <div class="card-body">
                    <form id=assign-admin class="" method="post"
                          action="">
                        <div class="form-group row row-bottom-margin">
                            <label for="userId" class="col-md-3 col-form-label form-control-label">User ID</label>
                            <div class="col-md-9">
                                <input name="userId" id="userId" class="form-control" type="number"
                                       required/>
                                <div class="invalid-input" id="user-error"><?php echo $userIdErr; ?></div>
                            </div>
                        </div>
                        <div class="form-group row row-bottom-margin">
                            <label for="assocId" class="col-md-3 col-form-label form-control-label">Association
                                ID</label>
                            <div class="col-md-9">
                                <select name="assocId" id="assocId" required>
                                    <option value="">None</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                </select>
                                <div class="invalid-input" id="user-error"><?php echo $assocIdErr; ?></div>
                            </div>
                        </div>
                        <div class="form-group row row-bottom-margin">
                            <label class="col-md-3 col-form-label form-control-label"></label>
                            <div class="col-md-9">
                                <input type="reset" class="btn btn-secondary" name="cancel" value="Cancel">
                                <input type="submit" name="summit" id="save-user" class="btn btn-primary"
                                       value="Submit">
                            </div>
                        </div>
                    </form>
                    <?php
//                    if ($updateAdminStatus == 1) {
//                        echo '<div class="alert alert-warning alert-dismissible fade show" role="alert">
//                                   dispalyError("assign an Admin");
//                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
//                                        <span aria-hidden="true">&times;</span>
//                                    </button>
//                                  </div>';
//                    }
                    if($updateAdminStatus == 1){
                        displaySuccess("Assign an admin successfully.");

                    }
                    ?>
                </div>
            </div>
        </div>
    </div>

    <?php
    include '../partials/footer.php';
    ?>
